# description succinte des données

## statures.csv

extrait d'un tableau d'inventaire des estimations de taille d'individus d'après des mesures sur des fémurs.

## inventaire_type_sep_region_centre.csv

coordination: Jérome Livet 

Tableau collaboratif pour la XIe rencontre du [gaaf 2019](https://gaaf11.hypotheses.org/tag/gaaf) 
L'échelle d'enregistrement est celle d'un type de sépulture pour un site et une période donnée.

## culot_blignicourt.csv

auteur: Benjamin Jagou

Table d'inventaire paléométallurgique de culots.     
l est possible de faire un diagramme ternaire avec les champs Longueur/Largeur/Epaisseur

## amboise_ceram_proto.csv

auteur: Francesca Di Napoli

Table d'inventaire de céramiques protohistoriques issues de l'*oppidum* d'Amboise.