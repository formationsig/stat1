# ![icon_mod2_stat.png](images/mod2_stat.png) module socle 1.1. Décrire, analyser et représenter ses données

## Rappel de l'épisode précédent

**pré-requis:** "Les données archéologiques : préparer et consolider ses tableaux"

Dans l'épisode 0, vous avez appris à trier un tableau, à manipuler les cellules et à utiliser quelques fonctions de base.

Dans l'épisode actuel, maintenant que les données sont "propres", nous allons travailler sur les données archéologiques en commençant par les décrire, puis en les analysant et enfin en les représentant, le tout dans un logiciel de tableur.

## Le logiciel utilisé

Nous avons fait le choix d'utiliser le logiciel[ LibreOffice Calc](https://fr.libreoffice.org/), Pour plusieurs raisons:
- Il est installé par défaut sur tous les ordinateurs Inrap.
-  C'est un logiciel libre (tout le monde peut l'installer gratuitement) et multi plateforme (Windows, Mac OS, Linux, etc).
- Il dispose de toutes les fonctions nécessaires à la préparation des données, à leur manipulation et à leur représentation graphique.
- Il possède les mêmes fonctionnalités qu'Excel et son interface minimaliste est proche des anciennes versions de celui-ci.
- Il peut importer et exporter tous les formats de fichiers courants (csv, dbf, xls, xlsx), et le format ods est compatible avec les autres tableurs.

 {% hint style='tip' %}

* Faire une copie des données depuis le dossier :file_folder: **STAT/donnees** dans un nouveau dossier :file_folder:STAT/**travail** 
* Ouvrir **LibreOffice Calc** puis le fichier :file_folder:**travail/donnees_archeo.ods**

{% endhint %}

## 1. Décrire ses données

:construction: Pour pouvoir y revenir au fil du déroulé, parler dès cette partie du/des données exemples: les :sheep: moutons de soay (le contexte, les données archéo,la nécessité du référenciel, l'objectif de la démarche statistique...) :construction:

### 1.1. Qu'est ce que la statistique ?

### Statistique

Discipline de la mathématique qui a pour objet les grands ensembles de données, leur compréhension et leur représentation. Le terme statistique, construction européenne, apparaît au XXVIIIème siècle en Allemagne d’après le mot italien *statista* qui signifie homme politique. Elle est alors définie comme un outil qui permet à un gouvernant de connaître ses gouvernés qu’il ne peut rencontrer 1 à 1 (elle est d’ailleurs toujours très largement utilisée dans la sphère politique). Les termes utilisés en statistique se rapportent donc logiquement à la gestion d’un état : population, individu, caractère…

### Statistique descriptive

La **statistique descriptive** ou **statistique exploratoire** ou **analyse de données** a pour but de résumer l'information contenue dans les données de façon synthétique et efficace. C'est l'objet de cette formation.

Elle suppose une population que l'on va résumer à l'aide d'indicateurs tels que la moyenne, la médiane ou l'écart-type...

Elle utilise des représentations des données sous forme de tableaux, de graphiques, voire de cartes quand il s'agit d'information localisée.

Elle permet de dégager les caractéristiques essentielles du phénomène étudié et de suggérer des hypothèses pour une étude ultérieure plus approfondie.

Les probabilités ont ici un rôle mineur.

On parle aussi de Statistique exploratoire en effet ont décrit ses données, on les explore en utilisant une série d'outil afin des les « comprendre », les analyser.

C'est bien une démarche que l'on effectue et non pas une recette que l'on applique.

### Statistique inductive

La **statistique inductive** ou **statistique inférentielle** ou **probabiliste**, permet d’utiliser des observations (faites sur des échantillons) pour présumer de résultats relatifs à une population.

> :smiley: "Ce qui a une *chance* sur un *million* d'*arriver* se produit *9 fois sur 10*". *Terry Pratchett*

Elle va au-delà de la description des données même si la statistique descriptive est souvent un préalable indispensable.

En général, il faut pouvoir comparer l'échantillon a un modèle probabiliste du phénomène aléatoire étudié et savoir estimer le risque d'erreurs.

Les probabilités jouent ici un rôle fondamental.

Rassurez vous ce n'est absolument pas l'objet de cette formation !

### Démarche statistique

1. Il s'agit avant tout de **choisir et collecter un échantillon d'individus** suffisamment représentatif de la population globale. Malheureusement, l'archéologue ne maîtrise pas son échantillonnage : il travaille avec se qu'il trouve !
    Ceci dit la collecte des données est évidemment la clé de voûte d'une étude statistique qui ne relève pas des mathématiques mais qui nécessite rigueur et abnégation :unamused:.

2. **On décrit ensuite l'échantillon** et on extrait le maximum d'information pour résumer ses caractéristiques : c'est l'objet de la statistique descriptive ou exploratoire.

3. Enfin, on peut chercher à savoir à quel point les caractéristiques de l'échantillon peuvent être représentatives des caractéristiques (inconnues!) de la population globale: c'est l'objet de la statistique inférentielle.

### statistique univariée, bivariée, multivariée

On distingue:

**- statistique univariée :** à chaque individu d'une population correspond un seul caractère / paramètre. On étudie, on décrit un seul paramètre, une seule colonne de notre tableau.

**- statistique bivariée ou multivariée :** à chaque individu d'une population correspond 2 (bi) plusieurs (multi) caractères ou paramètres pour lesquels on va chercher des corrélations.

### 1.2. Vocabulaire statistique ?

### Population $n$ 

En statistique, on travaille toujours sur une population statistique.

La population est l’ensemble des individus sur lesquels des observations et des mesures sont réalisées.

$n$ désigne le nombre d’individus d’une population en langage statistique.

>  exemples: les stagiaires de la formation stat, des céramiques, des tombes, des os, etc.

{% hint style='info' %} 

 La population doit être définie clairement. Quelles sont les sources et les critères qui définissent cette population ?

> Les stagiaires de la formation stat ? Est-ce une définition de population suffisante ?
>
> → Qu’est-ce qui définit un stagiaire de la formation stat ? (ceux ici présents ? Tous ceux, des 1000ers qui ont suivis cette formation ? Ceux qui sont inscrits, même s'ils ne sont pas venus ?...) (Clarté de la population)
>
> → À quelle date la liste des stagiaires a-t-elle été établie ? (importance du temps dans la définition de la population)

{% endhint %}

Cela revient à bien définir son échantillon.

### Echantillon

C’est un sous-ensemble de la population, réellement accessible à l’expérimentateur. Dans le cas des études archéologiques cet échantillon est souvent très réduit.

### Individu $i$

Cette population comprend un certain nombre d’individus ou d’unités statistiques (chaque fait, chaque céramique, pièces isolées….).

L’individu $i$ est une entité élémentaire sur laquelle on va mesurer ou observer des phénomènes.

Il correspond à une ligne et une seule dans le tableau. Il possède un identifiant unique (ID).

Une façon habituelle de procéder consiste à identifier chaque individu par un nombre entier compris entre 1 et n.

> exemple : une tombe étudiée : la 312, le tesson de bord de la dressel 654 qui fait 2 cm d'épaisseur.

{% hint style='working' %} 

$i$ désigne un individu quelconque en langage statistique.

{% endhint %}

### Variable ou caractère $X$

On appelle **caractère** ou **variable** une propriété commune à tous les individus d’une population statistique. Cette caractéristique ou propriété quelconque peut (ou non) différer d’un individu à l’autre.

On appelle variable une série de mesures ou d'observations effectuées sur un échantillon.

Elle correspond à une colonne et une seule dans le tableau.

> exemple : 
>
> - si on étudie un ensemble de squelettes d’une nécropole, on peut les décrire par leur sexe, leur taille, leur position, etc.
> - si on étudie un ensemble de silex, la nature pétrographique est un caractère que l’on peut observer, de même que la forme

{% hint style='working' %} 

$X$ désigne une variable quelconque en langage statistique.

{% endhint %}

### Modalité $x_1$

Une **modalité** est la valeur ou la situation prise par une variable pour un individu.

→ Un caractère présente plusieurs modalités.

→ Chaque individu statistique se caractérise par une modalité qui lui est propre.

{% hint style='working' %} 

On appelle  **modalité** la valeur prise par la variable pour un individu.:

- Ainsi, $x_1$ est la valeur prise par le 1er individu de la liste pour la variable $X$.
- et, $x_i$ est la valeur prise par l'individu  $i$ pour la variable $X$.

{% endhint %}

{% hint style='info' %} 

Les modalités sont :

-   **Incompatibles entre elles:** pour une variable $X$, un individu $i$ ne peut enregistrer qu'une   seule modalité $x_i$.
    
-   **Exhaustives:** tous les individus d'une population possèdent une modalité pour la variable), sinon on parle de valeur manquante.

> exemple : Tous les squelettes étudiés d'une nécropole n'ont pas la même taille (mais la taille de chaque squelette doit avoir été estimée pour être incluse dans l'analyse). Chacune des valeurs de taille est une modalité de la variable "taille".

{% endhint %}

{% hint style='tip' %} 

 **donnees_archeo.ods** 

Identifier dans les tableaux population/échantillon, individu, variable et modalité.
{% endhint %}

### Données et Tableau élémentaire

#### Données 

En statistique, les données désignent l’ensemble des individus observées (ceux de l'échantillon) + l’ensemble des variables considérées + les observations de ces variables sur ces individus (modalités). 

Les données sont en général présentées sous forme de **tableau élémentaire** et stockées dans un fichier informatique.

#### Tableau élémentaire

Il s’agit d’un **tableau à double entrée** où **les lignes correspondent aux individus** et **les colonnes aux variables** décrivant ces éléments.

{% hint style='info' %} 

La 1ère colonne est souvent réservée à la liste nominale des éléments sans que cela soit obligatoire.

{% endhint %}

Chaque ligne de ce tableau correspond à un individu pour lequel on dispose de valeurs observées. Chaque colonne correspond à une variable pour laquelle on a effectué des mesures. 

{% hint style='working' %} 
En langage statistique on présente le tableau élémentaire sous cette forme ↓
![tableau_elementaire](images/tab_elem.png) 

Chaque ligne de ce tableau correspond à un **individu** pour lequel on dispose de  $p$ valeurs observées. Chaque colonne correspond à une **variable** pour laquelle on a effectué $n$  mesures. 
Le double indice $(i,k)$ utilisé dans la notation $x_{ik}$ nous indique directement dans quelle ligne $(i)$ et quelle colonne $(k)$ se situe cette donnée.
{% endhint %}

### 1.3. Type / nature d'une variable

On voit à travers les exemples précédents qu’il existe plusieurs types de caractères. Chaque variable étudiée renvoie à un phénomène archéologique bien particulier.

Il peut s’agir de :
* phénomènes **concrets** (effectifs d’une population, production agricole, forme d'un objet..) ou **abstraits** (flux d’informations, de capitaux…)
* phénomènes  **quantifiables**   (effectifs de population d'une nécropole, dimensions d'un silex…)  ou **non-quantifiables** (datation, orientation, type de fait…)
* phénomènes **fixes** (effectifs à un temps $T$ …) ou **évolutifs** (évolution d’effectifs, de taux, tendances…)

Lorsque l’on travaille sur des données l’une des premières choses à vérifier est le type de variable auquel on est confronté. À chaque type de variable correspond des outils statistiques différents et des règles sémiologiques précises.

On classe les variables en 2 catégories :
* Les caractères Qualitatifs (QL ou q) ou facteur, les modalités expriment l'appartenance à une catégorie.
* Les caractères Quantitatifs (QT ou Q), les modalités s'expriment en nombres réels sur lesquels les opérations arithmétiques courantes ont un sens. Il est possible de les ordonner et de faire des calculs dessus.

![type_variable](images/type_variable.png)

:construction: voir la fiche technique de [détermination du type de variable](images/caractere_variable_semio-RV.pdf) :construction:

### Les variables qualitatives

#### Qualitative ordinale

On parle de qualitatif ordinal lorsque les modalités correspondent à des rangs (l’ordre des modalités à un sens, il possède une logique) 

> exemples : 
>
> * on classe les périodes selon un ordre chronologie qui a un sens (Préhistoire→Protohistoire→Antiquité).
> *  On peut classer des os selon un degré de conservation (TB→B→moyen→mauvais)

{% hint style='danger' %} 
Si une variable peut être exprimée sous la forme de nombres (genre 1-2-3-4) ce n'est pas pour autant un caractère quantitatif.
{% endhint %}

#### Qualitative nominale

On parle de qualitatif nominal lorsque l’ordre des modalités n’a pas de sens ; les modalités ne sont pas ordonnées entre elles.

> exemple : orientation de sépultures (nord-est, sud,...), présence/absence de carie sur une dent, interprétation d'un fait archéologique,...

#### Qualitative discrète

Un caractère qualitatif est discret lorsque le nombre de modalités est limité: **Il y a moins de modalités que d'individus**. 

>  exemple : type de faits (généralement liste limitée : TP, Fosse,.. ou période chronologique.

#### Qualitative exhaustive

Un caractère qualitatif est exhaustif lorsqu'il y a autant de modalités que d'individus. Exemple : difficile à trouver à part les ID genre le numéro de fait ?

{% hint style='info' %} 

Pour une variable catégorielle (qualitative) on appelle **facteurs** les différents modalités possibles de cette variable (même ceux qui ne sont pas représentés dans les données).

> exemple: pour la variable sexe: les différents facteurs sont Mâle, Femelle

{% endhint %}

### Les variables quantitatives

#### Quantitative absolue

On parle de **quantitatif absolu** (ou **de stock**) lorsque nous avons affaire à des nombres réels continus

>  exemple : une surface, une taille,...

Un caractère quantitatif absolu exprime des quantités concrètes: la somme des modalités des individus a un sens. Exemple : NR-NMI, diamètre, longueur, poids…

#### Quantitative relative

On parle de quantitatif relatif (ou d’intensité ou de taux ou de rapport) lorsque nous avons à faire à un rapport entre deux valeurs. 

> exemple : nombre de silex par m², taux de fragmentation en % 



{% hint style='info' %} 

Une variable est quantitative relative quand on peut calculer une moyenne mais que la somme n’a pas de sens. 
> exemple : on peut faire une moyenne des densité de silex par carré mais pas les additionner : 10 silex/m² dans un carré + 20 silex/m² dans un autre carré, ça fait pas 30 silex au m² !!!! Les modalités peuvent être ordonnées c'est à dire que l'on peut classer la densité de silex ou les taux de fragmentation.

{% endhint %}

#### Quantitative discrète

On parle de quantitatif discret quand les valeurs mesurées correspondent uniquement à un nombre fini de valeurs isolées (généralement des nombres entiers, mais pas forcément).

> exemple : nombre d'agents Inrap, nombre de structure dans une emprise, NR-NMI, nombre de ville de plus de 100000 h dans chaque département

#### Quantitative continue

La notion de quantitatif continu fait référence à l’échelle numérique des valeurs qui est celle des nombres réels. Les valeurs potentiellement prises par la variable sont en nombre infini. 

>  exemple : longueur d'un tibia, un diamètre, une taille, ... La moyenne des valeurs a alors un sens.

#### Mesurable

On parle de quantitatif mesurable quand on peut mesurer une modalité sur une échelle numérique. **Le 0 signifie bien l'absence du phénomène**.

> exemple : population, taux de fécondité, précipitations

#### Repérable

On parle de quantitatif repérable quand une modalité est repérable sur une échelle d'intervalle. Ces caractères permettent de repérer la position de chaque élément par rapport à une origine arbitraire. La valeur 0 est donc conventionnelle et ne signifie pas l'absence du phénomène. 

> exemple : latitude & longitude, température, altitude, …

## 2. Analyser et représenter les variables qualitatives

### 2.1 Tableau de dénombrement

La première étape du traitement statistique d'une variable qualitative discrète consiste à partir du tableau élémentaire pour construire un tableau de dénombrement.

|  ID  | profession  ($X$) |
| :--: | ----------------- |
|  1   | agriculteur       |
|  2   | menuisier         |
|  3   | agriculteur       |
|  4   | agriculteur       |
|  5   | marchand          |
|  6   | marchand          |
|  7   | agriculteur       |
|  8   | militaire         |
|  9   | marchand          |
|  10  | militaire         |

:construction_worker_woman: *tableau élémentaire contenant la variable "profession"*

| modalité de la variable ($x_i$) | effectif ($n_i$) |fréquence relative ($f_i$)|
| :------------------------------ | :--: | :--: |
| agriculteur                     | 4|40%|
| menuisier                       | 1|10%|
| marchand                        | 3|30%|
| militaire                       | 2|20%|
| **TOTAL:**                      | **$n$=10**|**100%**|

:construction_worker_woman: *tableau de dénombrement de la variable "profession"*



Pour cela il suffit, de lister:

1. dans une 1ère colonne les différentes modalités de la variable $X$ 
2. dans une 2ème colonne d'indiquer l'effectif $n_i$ des individus qui prennent cette modalité (=dénombrement)
3. et dans une 3ème colonne la fréquence relative $f_i=\frac{n_i}{n}$ (c'est à dire la part que représente l'effectif d'une modalité par rapport à l'effectif total) 

{% hint style='info' %} 
l'**effectif** est aussi appelé **fréquence absolue** et la **fréquence relative** est aussi appelée **fréquence simple**.
{% endhint %}

{% hint style='info' %} 
La fréquence relative d'une modalité :
* est toujours comprise entre 0 et 1 (ou 0 et 100 si elle est exprimée en %)  
* la somme des fréquences relatives est égale à 1 (ou 100)
{% endhint %}

{% hint style='tip' %} 
**donnees_archeo.ods** - feuille **mouton_soay**
Construire le **tableau de dénombrement** de la variable **"sexe"**. 
> **Pour le construire dans LibreOffice Calc: cf. module0#Le tableau croisé (ou TCD)**
>
> résultat ↓
>
> | modalité de la variable sexe ($x_i$) | effectif ($n_i$) | fréquence relative ($f_i$) |
> | :----------------------------------- | :--------------: | :------------------------: |
> | femelle                              |        84        |           56,5%            |
> | mâle                                 |        33        |            22%             |
> | castré                               |        32        |           21,5%            |
> | **TOTAL:**                           |   **$n$=149**    |          **100%**          |

> :sheep: *tableau de dénombrement de la variable sexe d'après le tableau élémentaire des os de moutons Soay*
> {% endhint %}

### 2.2 Représentations graphiques

:construction: prévoir une fiche technique Libreoffice Calc par représentation graphique :construction:

{% hint style='info' %} 
Il est important d'ajouter l'effectif total ($n$) à  toute représentation graphique, d'autant plus si l'on veut comparer plusieurs populations.
{% endhint %}

#### 2.2.1 Diagramme en bâton

À chaque modalité du caractère, on associe un bâton/rectangle dont la hauteur est proportionnelle à l’effectif.

![diagramme_en_barre](images/diag_barre.png)

* C'est la **représentation graphique normale d'un caractère discret**.
  * en **abscisse** = suite **ordonnée** des modalités
  * en **ordonnée** = effectif ou fréquence relative

{% hint style='danger' %} 
Dans un diagramme en barres on veut dissocier les modalités de la variables, **les Barres** sont donc **non-jointives** et **de largeur constante**.
{% endhint %}

{% hint style='info' %}
Il peut être intéressant de trier les modalités pour améliorer la lecture d'un diagramme en barre.
{% endhint %}

{% hint style='tip' %} 
**donnees_archeo.ods** - feuille **mouton_soay**
À partir du tableau tableau de dénombrement de la variable **"sexe"** des moutons de Soay, construire un **diagramme en barre**.
{% endhint %}

#### 2.2.2 Diagramme en secteur circulaire

**Objectif :** visualiser l’importance relative de chaque modalité plus que l’effectif lui-même
**Principe :** diviser un disque en secteurs angulaires dont les mesures (angles) sont proportionnelles aux effectifs (ou aux fréquences) de chaque modalité.

![camembert](images/camembert.png)

Ils permettent de visualiser des parts relatives, dans des surfaces ou secteurs de cercle, que l’on différencie par des couleurs ou des trames différentes.

Représentation équivalente au diagramme en bâtons mais moins performante sur le plan visuel. Bien qu’elle soit assez souvent utilisée, cette représentation est à exclure de vos pratiques, d'autant plus quand le nombre de modalités est importante et/ou que vous voulez comparer des graphiques entre eux.

> :smiley: "Un tableau est presque toujours mieux qu’un stupide graphique circulaire ; le seul modèle pire qu’un graphique circulaire est un modèle avec plusieurs d’entre eux […] les graphiques circulaires ne devraient jamais être utilisés ". Edward Tufte *in*  (The Visual Display of Quantitative Information)

{% hint style='tip' %} 
**donnees_archeo.ods** - feuille **mouton_soay**
À partir du tableau tableau de dénombrement de la variable **"sexe"** des moutons de Soay, construire un **diagramme circulaire** (ou éventuellement un camembert éclaté :cheese: ou un donut 3d :doughnut: ).
{% endhint %}

#### 2.2.4 Diagramme en barres empilées

Représentation consistant à découper une barre (représentant 100% de l’effectif) en segments dont la longueur est proportionnelle à l’effectif de chaque modalité.
Cette représentation n'est particulièrement intéressante dans le cas des caractères où il existe un ordre entre les modalités.

![barre_empile](images/barre_empile.png)

{% hint style='tip' %} 
**donnees_archeo.ods** - feuille **mouton_soay**
À partir du tableau tableau de dénombrement de la variable **"sexe"** des moutons de Soay, construire un **diagramme en barees empilées**.
{% endhint %}

#### 2.2.3 Diagramme en étoile

Le diagramme en étoile, ou diagramme de kiviat consiste à partir d’un point central de faire partir autant d'axes que de modalités et à leur donner une longueur proportionnelle à leur fréquence.

Cette représentation est particulièrement adaptée aux **caractères cycliques** et pour **comparer 2 séries** avec les mêmes modalités et des valeurs proches.

![kiviat](images/kiviat.png)

:{% hint style='info' %} 
Ce diagramme est idéal si les modalités ne sont pas trop nombreuses (mais supérieures à 2) et les **écarts d’effectifs** pas trop importants !
{% endhint %}

{% hint style='tip' %} 
**donnees_archeo.ods** - feuille **mouton_soay**
À partir du tableau tableau de dénombrement de la variable "sexe" des moutons de Soay, construire un **diagramme en étoile**.
{% endhint %}

### 2.3 Cas des variables quantitatives discrètes

On appelle variable quantitative discrète une variable quantitative ne prenant que des valeurs entières (plus rarement décimales). Le nombre de valeurs distinctes d’une telle variable est habituellement assez faible (sauf exception, moins d’une vingtaine). 

> exemple: le nombre d’enfants dans une population de foyers, le nombre d’années d’études après le bac dans une population d’étudiants.

:information_source: une variable **Quantitative Discrète** peut être assimilée à une variable Qualitative Discrète Ordonnée.

Le nombre de valeurs distinctes étant faible, on va pouvoir  présenter (sous forme de tableau de dénombrement / tableau statistique) et surtout représenter graphiquement une variable quantitative discrète de la même façon qu'une variable qualitative discrète.

## 3. Analyser et représenter les variables quantitatives

### 3.1 Tableau de distribution (début)

Dans le cadre des caractères continus, il est nécessaire de réaliser un tableau de distribution. Il s’agit d’un tableau élémentaire dans lequel les valeurs du caractère X ont été ordonnées en ordre croissant. 

Le tableau de distribution statistique est donc l'ensemble ordonné (par ordre croissant) des valeurs prises par un caractère quantitatif.

Le tableau de distribution statistique est donc un simple reclassement du tableau élémentaire.

{% hint style='tip' %} 
**donnees_archeo.ods** - feuille **mouton_soay**
Réaliser le tableau de distribution de la variable **"long_total"** (uniquement les fémurs) à partir du tableau élémentaire des mesures sur les os de moutons de Soay:

* Filtrer pour n'afficher que les individus correspondant aux fémurs.
* Créer une nouvelle feuille **<u>soay_femur</u>** 
* Copier la colonne **"long_total"** filtrer dans cette nouvelle feuille et la trier en ordre croissant.
{% endhint %}

### 3.2 Résumés numériques

Une série de données peut être résumée par quelques valeurs numériques appelées caractéristiques des séries statistiques, classées en 4 classes :
* les caractéristiques de tendance centrale (moyenne, médiane,...)	
* les caractéristiques de dispersion (écart-type, variance,...)
* les caractéristiques de formes (normale, unimodale, bimodale,...)
* les caractéristiques de concentration



{% hint style='tip' %} 
**donnees_archeo.ods** - feuille **<u>*soay_femur*</u>**

À partir de la variable **"long_total"** (feuille **<u>*soay_femur*</u>**),calculer au fur et à mesure du déroulé:
* Les caractéristiques de tendance centrale / de position:
La moyenne: ``=MOYENNE(plage)``
La médiane: ``=MEDIANE(plage)``
Le mode: ``=MODE(plage)`` :warning: Cette fonction ne retourne qu’une unique valeur, il vaut peut être mieux le(s) définir en observant la variable ordonnée.
* Les caractéristiques de dispersion:
L'étendue: ``=MAX(plage)-MIN(plage)``
La variance: ``=VAR(plage)`` :warning: le calcul se fait sur $n$-1 car basé sur un “échantillon”, sinon utiliser ``=VARP(plage)``
L'écart-type: ``=ECARTYPE(plage)`` :warning: même remarque, préférer ``=ECARTYPEP(plage)``
Les quartiles: ``=QUARTILE(plage; type)`` pour $Q1$ c’est donc ``=QUARTILE(plage; 1)`` → calculer les $Q1$, $Q2$, $Q3$
L'intervalle Inter-Quartile c'est $Q3-Q1$
{% endhint %}

#### 3.2.1 Caractéristiques de tendance centrale/de position

Elles donnent une idée de l'ordre de grandeur des valeurs constituant la série ainsi que la position ou semblent se concentrer les valeurs de cette série. Les principales caractéristiques de tendance centrale sont la moyenne arithmétique, la médiane, la médiale, le mode et les quantiles.

#### 3.2.1.1 La Moyenne $\bar x$

La moyenne arithmétique c'est la **somme de toutes les valeurs observées divisée nombre d’observations**.

Les propriétés de la moyenne:

* La moyenne arithmétique permet de résumer par un seul nombre la série statistique.
* Elle prend en compte toutes les valeurs de la série et elle est facile à calculer.
* Elle est sensible aux valeurs extrêmes, il est parfois nécessaire de supprimer des valeurs extrêmes ou « aberrantes ».

{% hint style='zob' %} 
Le chatisticien : “Quand Bill Gates rentre dans un bar, en moyenne tout le monde est millionaire!” 
{% endhint %}

{% hint style='tip' %} 
Démonstration: décomposition de la moyenne en langage mathématique formel.
{% endhint %}

#### 3.2.1.2 La Médiane $Me$ ou $Q2$

La médiane est la valeur, observée ou possible, dans la série de données classée par ordre croissant (ou décroissant) qui partage cette série en deux parties comprenant exactement le même nombre de données de part et d'autre de Me.

Son calcul a pour but de décomposer la distribution statistique afin qu’il y ait le même nombre d’observations « avant » et « après » la médiane (soit 50 % de l’effectif avant et 50 % après).

Les propriétés de la médiane :

- Le calcul/l'observation de la médiane est rapide.
- La médiane n'est pas influencée par les valeurs extrêmes ou aberrantes.
- La médiane est influencée par le nombre d'individus (de données) mais pas par leurs valeurs.
- Si la variable statistique est discrète, la médiane peut ne pas exister ; elle correspond seulement à une valeur possible de cette variable.
- La médiane est le point d'intersection des courbes cumulatives croissantes et décroissantes.
- La médiane ne se prête pas aux combinaisons arithmétiques ; la médiane d'une série ne peut pas être déduite des médianes des séries composantes.

#### 3.2.1.3 Le(s) Mode(s) $Mo$

Le Mode est la valeur de la variable statistique la plus fréquente que l'on observe dans une série d'observations.

Si la variable est une variable discrète, le Mode s'obtient facilement. Si la variable est une variable continue, on définit une classe modale.

Les propriétés du mode:

* Le Mode n'existe pas toujours et quand il existe, il n'est pas toujours unique.
* Si après regroupement des données en classes, on trouve deux ou plusieurs modes différents, on doit considérer que l'on est en présence de 2 ou plusieurs populations distinctes ayant chacune leurs caractéristiques propres ; dans ce cas, la moyenne arithmétique n'est pas une caractéristique de tendance centrale !

 {% hint style='info' %}  L’outil statistique de QGIS appelle le mode “Majorité” {% endhint %}

### 3.2.2 Caractéristiques de dispersion

Ces caractéristiques quantifient les fluctuations des valeurs observées autour des valeurs observées autour de la valeur centrale et permettent d'apprécier l'étalement de la série. Les principales sont : l'**écart-type** ou son carré la **variance**, l'**étendue** et l'**intervalle interquartile**.

#### 3.2.2.1 L'Étendue $E$

L'étendue c'est la différence entre la valeur maximum et la valeur minimale.
Les propriétés de l'étendue:
* L'étendue est facile à calculer.
* Elle ne tient compte que des valeurs extrêmes de la série ; elle ne dépend ni du nombre, ni des valeurs intermédiaires ; elle est très peu utilisée dès que le nombre de données dépasse 10.
* Elle est utilisée lorsque le nombre d'individus est de 4 ou 5 ; elle donne alors une idée appréciable de la dispersion. Cependant, dès que le nombre d'individus est de 15 à 20, il est préférable d'utiliser l'écart-type pour apprécier la dispersion.

#### 3.2.2.2 La Variance ($V$ ou $\sigma²$) et l'écart-type ($\sigma$)

La **variance** c'est **« La moyenne de la somme des carrés des écarts par rapport à la moyenne arithmétique. »**

{% hint style='working' %}

Si la définition peut effrayer, il suffit de décomposer la définition **« La moyenne de la somme des carrés des écarts par rapport à la moyenne arithmétique. »** en partant de la fin:

* Calculer la moyenne.

* Calculer pour chaque modalité, son écart à la moyenne (parfois les écarts seront positifs parfois négatifs).

* Mettre ces écarts au carré (pour qu'ils soient tous positifs et pour accentuer les écarts-importants).

* Faire la somme de ces écarts au carré.

* C'est une moyenne, il faut donc diviser le tout par l'effectif.

→ Bravo :clap: vous avez calculé la **Variance** !

* Calculer la racine-carré de la variance

→ Bravo :clap: vous avez calculé l'**écart-type** !
{% endhint %}

{% hint style='danger' %}

Comme le calcul de la variance se fait à partir du carré des écarts, les unités de mesure ne sont pas les même que celles des observations originales. Par exemple pour des tailles en cm la variance sera donc mesurée en cm2.

C'est pourquoi on utilise l'écart-type qui -comme il est la racine carré de la variance- s'exprime dans l'unité de mesure de l'échantillon.

{% endhint %}

La racine carrée de la variance est appelé **écart-type**. Il s’exprime dans l’unité.

L'Écart-type est la mesure de dispersion la plus couramment utilisée en statistique lorsque l'on utilise la moyenne pour calculer une tendance centrale.

L'Écart-type mesure donc la dispersion autour de la moyenne.

Il est très utile pour comparer 2 séries statistiques qui ont approximativement la même moyenne.

Plus l’écart-type est petit plus les données sont regroupées autour de la moyenne.

{% hint style='danger' %}
Il n'est pas toujours facile d'évaluer l'importance que doit avoir l'écart-type pour que les données soient largement dispersées. Il faut penser à l'unité de la variable.
> *Par exemple : Si on compare les recettes de 2 entreprises, un écart de 10000 ($) est peu significatif alors que pour des tailles de 2 populations, 50 (cm) c'est énorme !*

{% endhint %}

Les propriétés de l’Écart-type :
* On utilise l'écart-type uniquement pour mesurer la dispersion autour de la moyenne.
* L'écart-type n'est jamais négatif (basé sur un carré!)
* L'écart-type est sensible aux valeurs aberrantes (comme la moyenne)
* Si 2 séries ont la même moyenne, plus la dispersion est grande plus l'écart-type est grand.
* L'écart-type = 0 si toutes les modalités sont les même (car modalité = moyenne).

{% hint style='tip' %} 
Démonstration: décomposition de la variance et de l'écart-type en langage mathématique formel.
{% endhint %}

#### 3.2.2.3 Les quantiles ($Q$)

Les Quantiles sont des caractéristiques de position partageant la série statistique en $k$ parties égales.

Pour k=4, les quantiles appelées quartiles, sont 3 nombres $Q1$, $Q2$ et $Q3$ tels que :
 - 25% des valeurs prises par la série sont inférieures à $Q1$
 - 25% des valeurs prises par la série sont supérieures à $Q3$
 - $Q2$ est la médiane $Me$
	- $Q3-Q1$ est l'**intervalle interquartile**, il contient 50% des valeurs de la série.
	L’intervalle/la distance interquartile c’est l’équivalent de l’étendue pour les 50% centraux de la série statistique. 

Application de quartiles: la **boîte à moustache**:

![boxplot](images/boxplot.png)

Le diagramme en **boîte à moustache** ou **Boxplot** permet de représenter schématiquement les principales caractéristiques d'une distribution en utilisant les quartiles.

La partie centrale de la distribution est représentée par une boîte de largeur arbitraire et de longueur la distance interquartile, la médiane est tracée à l'intérieur. La boîte rectangle est complétée par des moustaches correspondant aux valeurs suivantes :

​	- valeurs supérieures : $Q3 + 1.5\times(Q3-Q1)$

​	- valeurs inférieures : $Q1  –  1.5\times(Q3-Q1)$

Les valeurs extérieures aux « moustaches » sont représentées par des étoiles (ou des oints) et peuvent être considérées comme des valeurs atypiques.

### 3.3 Représentations graphiques

Les représentations graphiques d’une variable continue ont toutes en commun de permettre d’explorer la distribution de la variable , en identifiant:
* la forme de la distribution (unimodale/courbe de gauss, bi ou multimodale, dissymétrique)
* les concentrations 
* les dispersions
* les ruptures dans la distribution



#### 3.3.1 La boîte à moustache

Elle est aussi appelée **diagramme en boîte**, **boîte de Tukey** (son inventeur en 1977) en français et ***boxplot***, ***box and whiskers plot*** en anglais.

C’est une représentation **unidimensionnelle** efficace pour visualiser une distribution:

- les minima et maxima et donc l’étendue (avec en plus des valeurs identifiées comme aberrantes ou atypiques).
- elle permet de répartir en la série en 4 parts égales en effectifs (la boîte par exemple encadre les valeurs mini et maxi pour les 50% d’individus au centre de la série.
- la position de la boîte nous donne une idée de l'asymétrie potentielle de la distribution.

Cette représentation est optimale pour comparer plusieurs distributions en figurant plusieurs boîtes à moustaches. Il est recommandé de signifier la moyenne à l’aide d’un point (ou d'un trait coloré) superposé.

![boxplot](images/boxplot.jpg)

#### 3.3.2 Le scalogramme

Il s’agit d’une représentation élémentaire et **unidimensionnelle** (il n’y a qu’un axe: celui des abscisses) d’une distribution statistique, consistant à représenter chaque élément de la distribution par un point sur un axe gradué.

![scalogramme](images/scalogramme.png)

Lorsque deux éléments ont des modalités identiques ou très proches, **on procède à un " empilement " des points**.

Le diagramme de distribution correspond alors à un histogramme utilisant de très petites classes d’effectifs égaux.

Il s’agit donc un schéma qui permet de visualiser l'ordre et la répartition des différentes valeurs d'une distribution statistique.

Construction du scalogramme:

- En abscisse, un axe horizontal orienté définit l'échelle de mesure du caractère. Chaque élément est positionné sur cette échelle par un point.
- Chaque valeur prise par le caractère est notée par un point rouge.
- L’axe des abscisses est gradué en fonction des valeurs du caractère.

{% hint style='tip' %}

Le scalogramme n'est pas un graphique implémenté dans les tableurs, il faut donc faire quelques manipulations de données.

> :warning: Ce graphique est basé sur la multiplication des occurences d'une valeur, par conséquent si les valeurs continues ont trop de chiffres derrière la virgule, le scalogramme risque d'être "aplati" → pour éliminer ce risque il est possible au préalable d'arrondir les valeurs afin de multiplier les redondances en utilisant la formule ``=ARRONDI(cellule ; nombre_de_chiffre_après_la_virgule)``

- **Vérifier** que la colonne A contenant la variable continue est triée par ordre croissant.

- Dans la colonne B taper la **formule de recherche de doublon** ``=NB.SI(A2:A$15;A2)``

- **Dérouler la formule**

  > :warning:  <u>explication</u>: pour un triplon, on veut qu’il marque 1, 2, 3 en face des différentes occurrences (pour empiler les points) . Il faut donc bloquer la dernière ligne (avec $) mais pas la cellule de départ. 

- **Sélectionner les 2 colonnes**.

- Cliquer sur l’icône Graphique ![icon_graph](images/icon_graph.png)

- Choisir **XY (Dispersion)**

- Choisir **Points Seuls**.

- Bouton **[Terminé]**

- **<u>Habillage</u> :**

  - **Supprimer la légende**: Clic dessus + [suppr]
  - **Modifier l'Axe Y**: en 2x clic dessus → Échelle → décocher tous les automatique Puis mini=1, maxi=valeur entière, intervalle principal=1, nombre d’intervalles secondaires=1 > [OK]
  - **Supprimer l’Axe Y**: Clic dessus → [suppr]
  - **Supprimer les lignes horizontales**: 2x clic sur une ligne → Style: -  aucun(e) -
  - **Réduire la hauteur de la fenêtre et l'élargir**

{% endhint %}

{% hint style='tip' %}

**donnees_archeo.ods** - feuille **<u>*soay_femur*</u>**

À partir de la variable **"long_total"** :

* arrondir la variable à 0 chiffres derrière la virgule.
* créer le **scalogramme**.

{% endhint %}

Le scalogramme est une représentation efficace, un diagramme de distribution qui permet une première appréhension de la distribution des valeurs d'un caractère :

- visualisation des valeurs maximum et minimum	
- visualisation des zones de concentration (valeurs rapprochées les unes des 	autres)	
- visualisation des zones de dispersion (valeurs éloignées les unes des autres)	
- visualisation des discontinuités de la distribution (zones de concentration 	séparées par un intervalle où les valeurs sont absentes).

{% hint style='info' %} Le scalogramme, contrairement à la boîte à moustache représente chaque individu, il peut être utilisé en association à cette dernière pour améliorer la lecture de distribution {% endhint %}

{% hint style='info' %} Le scalogramme, permet de distinguer les ruptures dans la distributions, les seuils naturels. Il peut par conséquent être utile pour choisr des bornes de classes dans le care d'une discrétisation d'une la variable quantitative continue. {% endhint %}

#### 3.3.3 Diagramme en tige et feuille

Il existe une autre manière de représenter la distribution à l'instar du scalogramme avec en plus la possibilité d'identifier les modalité / les valeurs que prend chaque individu pour la variable continue étudiée : c'est le diagramme en feuille.

Le diagramme en tiges et feuilles (*stem and leaf* en anglais) ou tracé en arborescence a lui aussi été inventé par John Tukey . A mi chemin entre le tableau et le graphique ce diagramme est un moyen de présenter toutes les données d’une étude statistique pour en faciliter la lecture d’ensemble.

>  <u>exemple fictif</u> avec une variable taille des stagiaires en cm.
>
>  **15 | 89**
>  **16 | 22568**
>  **17 | 123334457789**
>  **18 | 111112222233344588**
>  **19 | 001**

Le tracé indique comme tige : les premiers chiffres du nombre (milliers, centaines, dizaines...) et comme feuille : le dernier chiffre (unique) de ce nombre.
Les feuilles sont rangées par ordre croissant à l’intérieur d’une tige. Les tiges sont elles même rangées par ordre croissant.

**Le diagramme revient donc à regrouper les données par classe de même amplitude tout en y faisant figurer l’intégralité des données.**

Les avantages d’une telle présentation sont multiples :

- Toutes les valeurs y sont nommées et ordonnées ; 		
- Ce tracé ressemble quand on le tourne à un diagramme en bâtons ; 		
- On peut y ajouter l’effectif de chaque tige. 

- L’élève peut y lire facilement le nombre de données, la valeur la plus grande, la plus petite, la plus fréquente ainsi que les éventuelles valeurs aberrantes. 	
- <u>Bonus</u> : on peut comparer 2 séries statistiques (modalités (feuilles) d'un côté et de l'autre de la tige!)

{% hint style='info' %} Ce type de diagramme est couramment utilisé au Japon pour les horaires de trains {% endhint %}


#### 3.3.4 La courbe de fréquences cumulées

**Les courbes des fréquences cumulées ascendantes et descendantes** sont établies à partir du tableau de distribution statistique.

C'est un graphique bi-dimensionnel représentant en abscisse les modalités du caractère continu étudié et en ordonnée, les fréquences cumulées.

La construction élémentaire de la courbe des fréquences cumulées consiste à associer à chaque modalité du tableau de distribution statistique sa fréquence cumulée ascendante (% des éléments ayant des modalités de valeur inférieure ou égale) et sa fréquence cumulée descendante (% des éléments ayant des modalités de valeur supérieure ou égale)

On représente alors sur le graphique les modalités de la variable étudiée (en abscisse) avec leur fréquence cumulée ascendante ou descendante (en ordonnée)

![courbe_freq_cum](images/courbe_freq_cum.png)

{% hint style='tip' %}

La courbe des fréquences cumulées ascendantes (et descendantes) n'est pas implémentée dans les tableurs, il faut construire un tableau:

|      |     A      |           B            |                C                 |                 D                 |
| :--: | :--------: | :--------------------: | :------------------------------: | :-------------------------------: |
|  1   | **taille** | **frequence_relative** | **frequence_cumulée_ascendante** | **frequence_cumulée_descendante** |
|  2   |    158     | **``= A2/A$16*100``**  |           **``= B2``**           |              **100**              |
|  3   |    162     |         6,844          |        **``= C2 + B3``**         |              93,325               |
|  4   |    163     |         6,886          |              20,405              |              86,481               |
|  5   |    163     |         6,886          |              27,291              |              79,595               |
|  6   |    164     |         6,929          |              34,22               |              72,709               |
|  7   |    165     |         6,971          |              41,191              |               65,78               |
|  8   |    167     |         7,055          |              48,246              |              58,809               |
|  9   |    168     |         7,098          |              55,344              |              51,754               |
|  10  |    168     |         7,098          |              62,442              |              44,656               |
|  11  |    170     |         7,182          |              69,624              |              37,558               |
|  12  |    175     |         7,393          |              77,017              |              30,376               |
|  13  |    178     |         7,520          |              84,537              |              22,983               |
|  14  |    183     |         7,731          |              92,268              |       **``=  D15 + B14``**        |
|  15  |    183     |         7,731          |             **100**              |           **``= B15``**           |
|  16  |  **2367**  |                        |                                  |                                   |

1. <u>Colonne A</u>: **Trier** la variable quantitative continue en ordre croissant.
2. <u>Colonne B</u>: Calculer la **fréquence relative** que représente chaque valeur par rapport à la somme de toutes les valeurs).

> en B2, taper ``=A2/A$16*100`` puis dérouler vers le bas

3. <u>Colonne C</u>: Calculer la **fréquence cumulée ascendante** c'est à dire qu'en partant de l'individu à la plus petite valeur, ajouter la fréquence relative de chaque individu à la somme des fréquences relatives des individus précédents. le dernier individu doit être à 100%.

>en C2, taper ``=B2`` et en B3 taper ``=C2+B3`` puis dérouler vers le bas

4. <u>Colonne D</u>: Calculer la **fréquence cumulée descendante** c'est à dire qu'en partant de l'individu à la plus grande valeur, ajouter la fréquence relative de chaque individu à la somme des fréquences relatives des individus précédents. le premier individu doit être à 100%.

> en D15, taper ``=B15`` et en D14 taper ``=D15+B14`` puis **dérouler vers le haut**

{% endhint %}

{% hint style='tip' %}

Reste à faire le graphique:

1. sélectionner les 3 colonnes:
   * Colonne A: les valeurs :warning: ne pas sélectionner le total
   * Colonne C: les fréquences cumulées ascendantes
   * Colonne D: les fréquences cumulées descendantes
2. Cliquer sur l’icône Graphique ![icon_graph](images/icon_graph.png)
3. Choisir **XY (Dispersion)**
4. Choisir **Lignes Seuls** ou **Points et Lignes** .
5. Bouton **[Terminé]**
6. **<u>Habillage</u> :**

   - **Supprimer la légende**: Clic dessus + [suppr]
   - **Modifier l'Axe Y**: en 2x clic dessus → Échelle → décocher tous les automatique Puis mini=0, maxi=100, intervalle principal=10, nombre d’intervalles secondaires=1 → [OK]
   - **Ajuster la taille de la fenêtre**

{% endhint %}

{% hint style='tip' %}

**donnees_archeo.ods** - feuille **<u>*soay_femur*</u>**

À partir de la variable **"long_total"** :

* Créer le tableau des fréquences cumulées.
* Réaliser la courbe de fréquences cumulées.

{% endhint %}

Le diagramme de distribution et la courbes des fréquences cumulées permettent de repérer les **zones de concentration** de la distribution (beaucoup d’éléments sur un intervalle) et les **zones de dispersion** (peu d’éléments sur un intervalle). 

On peut également repérer des **discontinuités**, c’est-à-dire des zones de dispersion séparant deux zones de concentration des éléments.



Sur la courbe des fréquences cumulées :

- Les pentes fortes correspondent aux zones de concentration des valeurs
- Les pentes faibles correspondent aux zones de dispersion des valeurs
- Les " marches d’escalier " correspondent aux discontinuités de la distribution
- en plus il est possible de lire graphiquement les quartiles en reportant sur l’axe des abscisses 



Ce type de représentation graphique peu commune dans les disciplines archéologique est cependant intéressante car elle permet de comparer plusieurs distributions. 

#### 3.3.5 L'histogramme

L’Histogramme ne doit pas être confondu avec le diagramme en bâton:
le diagramme en barre est une représentation graphique d’une variable discrète:

* les barres sont disjointes car on veut représenter une différence entre les différentes modalités.
* c’est la hauteur des barres qui indique l’effectif pour chaque modalité.
l’histogramme est une représentation graphique d’une variable continue:
* les barres sont jointives car justement la série de donnée est continue!
* selon la construction de l’histogramme ce n’est pas la hauteur des barres qu’il faut prendre en compte mais la surface de celles-ci!

L’histogramme est une simplification/généralisation de la série statistique en regroupant dans des classes des valeurs proches. On appelle cela une partition en classe OU une discrétisation : transformer une variable continue en variable discrète.

La construction d’un histogramme est une démarche qui peut paraître longue et laborieuse mais qui permet de comprendre sa lecture.

Il faut d’abord définir le nombre de classes
Une méthode de découpage des classes
Puis construire un tableau de dénombrement
Et enfin réaliser l’histogramme

#### 3.3.5.1 Définir le nombre de classes

:construction: 

#### 3.3.5.2 Histogramme (classes d'amplitudes égales)

La représentation graphique la plus commune est l'histogramme (:warning:à ne pas confondre avec le diagramme en barre !):

* Sur l'axe des abscisses, on reporte les limites de classes qui servent de base à des rectangles contigus.

* Sur l'axe des ordonnées on reporte les effectifs de chaque classe qui servent de sommet aux rectangles.



> Par <u>exemple</u>, si on partitionne en 3 classes d'amplitude égales la variable quantitative continue taille d'un échantillon fictif de $n$=10 individus de 160 à 189 cm (cf.[diaporama](https://slides.com/archeomatic/statl2#/12/1)) on obtient le tableau de distribution statistique suivant ↓
>
> | **taille (classes)** | amplitude de classe | **centre de classe** | **effectif** | **fréquence relative** |
> | :------------------: | :-----------------: | :------------------: | :----------: | :--------------------: |
> |    **[160-170[**     |         10          |         165          |      3       |          30 %          |
> |    **[170-180[**     |         10          |         175          |      4       |          40 %          |
> |    **[180-190[**     |         10          |         185          |      3       |          30 %          |
> |                      |                     |     **TOTAL =**      | $n=$ **10**  |       **100 %**        |
>
> 
>
> 
>
> à partir duquel on peut construire l'histogramme suivant (avec les effectifs en ordonnée ↓
>
> ![hist_stature](images\hist_stature.png)
>
> ***histogramme de la variable taille d'un échantillon fictif de $n$=10 individus de 160 à 189 cm (cf.[diaporama](https://slides.com/archeomatic/statl2#/12/1))***

**:warning: Ce qu'il faut retenir:**

* Les rectangles sont contigus car la variable est continue.
* L'aire des rectangles est proportionnelle à l'effectif.
* Il faut faire attention à l'amplitude des classes: sont elles égales ?



* **Polygone des effectifs (ou des fréquences) : "allure" d'une distribution)*

A partir du tableau de distribution statistique avec des classes d'amplitude égale, il est possible de construire le polygone des effectifs (ou des fréquences):

* Sur l'axe des abscisses on repère les centre de classes.

* Au sommet de chaque rectangle on reporte le centre de classe.

* On relie tous ces points

:information_source: Le polygone des effectifs (fréquences) permet de visualiser l'allure d'une distribution statistique.



> <u>exemple</u>, si on reprend l'histogramme précédent, on peut construire le polygone des effectifs suivant ↓
>
> ![poly_stature](images\poly_stature.png)



**:warning: Ce qu'il faut retenir:**

* Le polygone des effectifs (ou des fréquences si ce sont celles-ci qui sont reportées sur l'axe des ordonnées) permet de visualiser l'allure d'une distribution statistique
* L'aire du polygone des effectifs = l'aire de l'histogramme = proportionnel à l'effectif total de la population
* On ne peut construre le polygone des effectifs (fréquences) que si les classes sont d'amplitude égale !



#### 3.3.5.3 Histogramme (classes d'amplitudes inégales)

:warning: Dans le cas de **classes d'amplitudes inégales on ne peut pas reporter les effectifs sur l'axe des ordonnées** mais les **densités d'effectifs  c'est à dire pour chaque classe le rapport entre l'effectif et l'amplitude de classe.**

> On note la densité d'effectifs $d_i$ tel que $d_i=\frac{n_i}{a_i}$ pour la densité d'effectif et  $d_i=\frac{f_i}{a_i}$ pour la densité de fréquence (Avec $a_i$ l'amplitude de la classe n° $i$  )



**:information_source: On utilise plus communément la densité de fréquence (*density*) c'est à dire le rapport entre la fréquence relative et l'amplitude que l'on reporte sur l'axe des ordonnées. Ainsi, la surface totale des rectangles qui composent l'histogramme est égale à 1.**

> Ainsi pour la variable **"duree_regne_an"** les classes définies ne sont pas d'amplitude égale, on peut donc compléter le tableau de distribution avec le calcul de la densité d'effectif et la densité de fréquences. ↓

| **duree_regne_an** | **amplitude de classe $(a_i)$** | **centre de classe** | **effectif** | **fréquence relative $(f_i)$** | **densité d'effectif** | **densité de fréquence $(d_i=f_i/a_i)$** |
| :----------------: | :-----------------------------: | :------------------: | :----------: | :----------------------------: | :--------------------: | :--------------------------------------: |
|     **[0-5[**      |              **5**              |         2,5          |      11      |            **0,42**            |       11/5 = 2,2       |            0,42/5 = **0,084**            |
|     **[5-10[**     |              **5**              |         7,5          |      2       |            **0,08**            |       13/5 = 0,4       |            0,08/5 = **0,016**            |
|    **[10-20[**     |             **10**              |          15          |      9       |            **0,35**            |       9/10 = 0,9       |           0,35/10 = **0,035**            |
|    **[20-41[**     |             **21**              |         30,5         |      4       |            **0,15**            |       4/21 = 0,2       |           0,15/21 = **0,007**            |
|                    |                                 |     **Total :**      |    **26**    |             **1**              |                        |                                          |

>  En effet on aurait pu être tenté de construire l'histogramme suivant ↓
>
>  ![hist_regne_fx1_X](images/hist_regne_fx1_X.png)
>
>  :warning: **Cet histogramme est faux** car les limites de classes ne sont pas reportées correctement sur l'**axe des abscisses** (même écart entre 5 et 10 et entre 20 et 21!)
>
>  Ou encore celui-ci, en corrigeant l'axe des abscisses ↓
>
>  ![hist_regne_fx2_X.png](images/hist_regne_fx2_X.png)/>
>
>  :warning: **Cet histogramme est faux** car **l'aire des rectangles doit être proportionnelle à l'effectif / la fréquence** (or, par exemple, les 4 empereurs qui ont régnés plus de 20 ans représentent une aire aussi grande que les 11 empereurs qui ont régnés moins de 5 ans !)
>
>  **→ C'est pourquoi il est nécessaire de calculer la densité d'effectif (ou de fréquence) et de la reporter sur l'axe des ordonnées.**



:construction: La construction de l'histogramme en cas de classes d'amplitude inégales, se fait ainsi:

* Sur l'axe des abscisses, on reporte les limites de classes qui servent de base à des rectangles contigus.

* Sur l'axe des ordonnées on reporte la densité de fréquence (ou éventuellement d'effectif) de chaque classe qui sert de sommet aux rectangles.

![hist_regne](images/hist_regne.png)

*Histogramme représentant la durée de règne (en années) des $n=$26 empereurs romains (depuis les Julio-Claudiens jusqu'aux Sévères)*.



**:warning: Ce qu'il faut retenir:**

* Dans le cas de classes d'amplitudes inégales c'est la densité de fréquence que l'on reporte sur l'axe des ordonnées
* **L'aire totale des rectangles de l'histogramme est de 1**.
