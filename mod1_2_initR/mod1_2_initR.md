# Les encarts

Dans ce déroulé vous trouverez 4 types d'encarts:

{% hint style='info' %}
Information : indique une information d'ordre général
{% endhint %}

{% hint style='danger' %}
Attention : indique une information importante ou un problème récurrent
{% endhint %}

{% hint style='tip' %}
Astuce : indique une astuce, un raccourci
{% endhint %}

{% hint style='working' %}
Exercice : indique des manipulations à faire ou l'énoncé d'un exercice
{% endhint %}





## Rappel de l'épisode précédent

Le pré-requis est "Les données archéologiques : préparer et consolider ses tableaux"

Dans l'épisode 0, vous avez appris à trier un tableau, à manipuler les cellules et à utiliser quelques fonctions de base.

Dans l'épisode 1, vous avez appris à travailler sur les données archéologiques en commençant par les décrire, puis en les analysant et en les représentant, le tout dans un logiciel de tableur. 

Dans l'épisode actuel, vous allez faire la même chose, mais dans un logiciel dédié aux statistiques. 



## Préambule : qu'est-ce que R ?

R est à la fois un langage et un environnement de travail dans le domaine des statistiques. Il permet de visualiser et exploiter de très larges quantités de données pour obtenir des statistiques mais pas seulement : vous pouvez à partir de R créer des graphiques et même des cartes. 

R est un projet open source et collaboratif, cela implique qu'il existe une très large communautés d'utilisateurs et de développeurs, dans tous les domaines imaginables. Les ressources en ligne sont très riches et nous vous en proposons [une sélection en bas de ce support](# Ressources). 


:warning: Attention, le logiciel et les commandes sont en Anglais. 

Dans cette initiation à R, nous allons découvrir le logiciel via l'une de ses interfaces - RStudio, apprendre à y intégrer des données, créer des graphiques et obtenir des statistiques simples. 



# 1. Prise en main

### 1.1. Installer R et RStudio

https://www.r-project.org/

R se télécharge via l'un des sites qui l'héberge dont la liste est [disponible ici](https://cran.r-project.org/mirrors.html). 

https://www.rstudio.com/products/rstudio/download/

Il faut **installer R en premier**, puis **RStudio** qui est le logiciel que nous allons utiliser ici pour son interface un peu plus accueillante.

### 1.2. L'interface de RStudio

![interface](images/interface_1.png)

A l'ouverture, RStudio se présente sous la forme d'un ensemble de fenêtres : à gauche, la console, à droite l'environnement et les dossiers. Chacune de ces fenêtres - ou onglets - peut être déplacée ou réduite selon les besoins. Si une fenêtre est fermée par erreur, elle peut être récupérée dans le menu *View* → *Panes* 

### Installer des packages

Pour installer une extension, si on dispose d’une connexion Internet, on peut utiliser le bouton *Install* de l’onglet *Packages* de RStudio.



### 1.4. Organiser son travail

#### a. Les projets

Les projets dans R regroupent l'ensemble des données nécessaires à une analyse statistique (un peu comme dans QGIS). Pour cela aller à *File* → *New project* 

![](images/interface_new_project.png)

On peut alors choisir de créer un nouveau dossier *New Directory* ou travailler dans un dossier existant *Existing Directory*. Ici nous allons travailler dans le dossier R créé dans le disque C : *Existing Directory* → *Browse* → *Create Project*

![](images\interface_new_project2.png)

#### b. Les scripts

Un script est une façon simple et efficace de mémoriser son processus de travail et d'être capable de renouveler les analyses lancées. 

Menu *File* → *New file* → *R script*

Pour ajouter un commentaire, il suffit de le faire précéder d’un ou plusieurs symboles `#`. Dès que R rencontre ce caractère, il ignore tout ce qui se trouve derrière, jusqu’à la fin de la ligne. 

{% hint style='tip' %} Il est **crucial** d'alimenter ses scripts au fur et à mesure de l'avancée du travail. {% endhint %}



Lorsque le script devient un peu long, on peut créer des sections qui, assemblées, forment un plan du script, accessible à droite. Pour cela, il faut commencer un commentaire et terminer par au moins quatre tirets 

```R
#1. Découverte de R ----
```

 ![alimenter un script](images/script_1.png)



Les sections peuvent être réduites en cliquant sur le petit triangle noir qui suit le numéro de ligne :

![section dans les scripts](images/script_2.png)



## Première approche de R

Avant de commencer : l'invite de commandes `>` située en début de ligne dans la console ; elle indique que le logiciel attend une commande. 

```
> 2+2
[1] 4
```

Le logiciel sait faire des calculs arithmétiques de base. On note au passage que la commande que l'on a saisie s'affiche dans le script. 

### Les objets dans R

Il existe de nombreux objets dans R, nous n'en verrons qu'une partie : les vecteurs et les dataframes principalement.

**L'opérateur d'assignation**

 `<-` Cette “flèche” stocke ce qu’il y a à sa droite dans un objet dont le nom est indiqué à sa gauche.

```R
x <- 25
y <- 3
```

On peut afficher le contenu de l'objet en tapant son nom puis `Entrée`:

```R
> x
[1] 25
```

{% hint style='danger' %} Attention, quand on assigne une nouvelle valeur à un objet existant, cela écrase la valeur précédente. {% endhint %}

On peut aussi utiliser les deux objets de type nombre pour effectuer un calcul de base : 

```
> x+y
[1] 28
```

**Les vecteurs **

Un *vecteur* dans R est un objet qui peut contenir plusieurs informations du même type, potentiellement en très grand nombre. Pour créer un vecteur, on utiliser la fonction `c` (pour *combine*) : on peut combiner des données qualitatives ou quantitatives, à condition de conserver le même type dans le vecteur. 

Exemple avec la taille en cm des stagiaires :

```R
tailles <- c(167, 186, 172, 184, 162, 166, 193)
```

Si on affiche le vecteur : 

```R
> tailles
[1] 167 186 172 184 162 166 193
```



**Gérer les valeurs manquantes **

Il arrive que des valeurs soient manquantes dans un tableau, dans ce cas elles seront affichées avec l'abréviation *NA* pour *non available*. 

{% hint style='danger' %} Attention ! certaines fonctions ne renvoient pas de résultat s'il y a des valeurs manquantes, c'est le cas par exemple du calcul de la moyenne. Il faut alors demander au logiciel de ne pas prendre en comptes les valeurs NA au moyen de l'argument `na.rm = TRUE` {% endhint %}



**Les fonctions**

Une fonction a un *nom*, elle prend en entrée entre parenthèses un ou plusieurs *arguments* (ou *paramètres*), et retourne un *résultat*.

Par exemple avec la fonction `View` qui permet d'afficher un tableau : le tableau à afficher est entre parenthèses : 

```R
View(tableau)
```

### Définir le répertoire de travail

Le répertoire de travail, c'est le dossier dans lequel R ira, par défaut, chercher les fichiers à importer et y exportera les documents produits (tables, graphiques, scripts enregistrés). 


```R
setwd("C:/mod_1.2/donnees_exercices")
```

Le chemin absolu vers le répertoire de travail est à mettre entre "guillemets". 	

{% hint style='tip' %} Charger le fichier *bavay_stature_sexe.csv* après avoir vérifié en amont le séparateur de colonnes (ici le point-virgule) et le séparateur de décimales (ici la virgule), au moyen de la fonction `read.csv2()` 

Si votre fichier a des valeurs séparées par des virgules, vous utiliserez la fonction `read.csv()` {% endhint %} 

```R
tableau <- read.csv2("C:/mod_1.2/donnees_exercices/bavay_stature_sexe.csv")
```

Notez que nous avons affecté l'objet **tableau** à l'action permettant de lire/d'interpréter le tableau stature.csv ; cela aurait pu se faire en deux fois.

:warning: :exclamation: si vous avez des erreurs d'encodage (caractères accentués qui ne passent pas), il faut préciser l'encodage utilisé au moment de faire monter le tableau : 

```R
tableau <- read.csv2("C:/mod_1.2/donnees_exercices/bavay_stature_sexe.csv", encoding = "UTF-8")
```



L'ouverture du fichier csv peut se faire sans ligne de commande en passant par la partie *Files* au moyen d'un clic gauche sur le document puis *Import dataset* : 

![import data set 1](images/import_dataset.png)

Il faut dans ce cas paramétrer quelques éléments comme le délimiteur (ici Semicolon pour le point-virgule),

![import data set 2](images/import_dataset_2.png)

et le séparateur décimal, ici la virgule 

![import data set 3](images/import_dataset_3.png)

Il faut ensuite lui assigner la variable tableau :

```R
tableau <- bavay_stature_sexe
```

```R
View(tableau)
```

Pour vérifier l'importation du tableau et regarder la structure, il est toujours utile d'utiliser la fonction `str`(). Elle renvoie un descriptif plus détaillé de la structure du tableau. Elle liste les différentes variables, indique leur type et affiche les premières valeurs.

```R
str(tableau)
```

```R
spc_tbl_ [54 × 3] (S3: spec_tbl_df/tbl_df/tbl/data.frame)
 $ fait   : num [1:54] 2069 2070 2071 2072 2087 ...
 $ sexe   : chr [1:54] "I" "I" "I" "I" ...
 $ stature: num [1:54] 161 NA NA NA 148 ...
 - attr(*, "spec")=
  .. cols(
  ..   fait = col_double(),
  ..   sexe = col_character(),
  ..   stature = col_double()
  .. )
 - attr(*, "problems")=<externalptr> 
```

{% hint style ='info'%} Chaque colonne du tableau représente une variable, indiquée par le signe $ dans la liste. {% endhint %}

Le tableau peut être affiché en cliquant sur l’icône en forme de tableau à droite du nom de l’objet dans l’onglet *Environment*.
![Environnement](images/tableau_environnement.png)

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

# Bref rappel sur les types de variables

![](images/type_variable.png)

Une variable **qualitative** présente des caractéristiques et non des quantités. Les valeurs que peut prendre cette variable sont appelées des catégories ou des modalités (dans R ce sont des *levels*). Elle peut être : 

- Nominale : un matériau, une couleur, un type d'objet...
- Ordinale ou ordonnée : petit, moyen, grand

On ne peut pas faire de calculs sur une variable qualitative (la moyenne d'un matériau n'a pas de sens). En statistique, c'est une variable **discrète** : elle ne peut prendre qu'un nombre précis de valeurs et ne sera représentée graphiquement que par un nombre limité de graphiques, principalement le diagramme en barres et ses dérivés. 



Une variable **quantitative** exprime une grandeur ou quantité. Elle peut être :

- **absolue** : c'est-à-dire résultant d'une observation (exemple : un effectif dans une population)
- **relative** : c'est-à-dire résultant d'un calcul (exemple : densité de tessons au m²)

En statistique, il sera surtout important de savoir si cette variable quantitative est **discrète** ou **continue** :

- **discrète** : elle a un nombre fini de valeurs possibles (exemple : le sexe des individus)
- **continue** : elle *peut* prendre toutes les valeurs possibles (exemple : la stature d'un groupe d'individus)

Les variables quantitatives sont des variables numériques sur lesquelles on peut faire des calculs ainsi qu'observer et décrire la dispersion et la tendance centrale. Elles sont représentées par des graphiques qui vont mettre en lumière cette dispersion.

<div style="page-break-after: always;"></div>

# 2. Les variables qualitatives (ou discrètes)

## 2.1. Décrire les variables qualitatives

La première chose à faire est de faire un tableau de dénombrement à partir de notre tableau élémentaire. Cela consiste à décompter le nombre d'occurrences pour chaque modalité. 
Le tableau de dénombrement est effectué avec la fonction `table()`

```R
table(tableau$sexe)
```

```
 F  I  M 
 7 32 15 
```

{% hint style ='info' %} Notez la syntaxe pour désigner la variable sexe sous la forme *nom_du_tableau$nom_de_la_variable* {% endhint %}

Il peut être utile ici:

* de trier le résultat par ordre croissant avec la fonction `sort()`
* d'affecter un nom d'objet (**tab.den**) au résultat pour s'en servir par la suite

```
 tab.den <- sort(table(tableau$sexe))
```

Les fonctions peuvent être emboitées comme on le voit ci-dessus.

On peut obtenir ce même tableau avec les fréquences (pourcentages) avec la fonction `prop.table()` : 

```R
> prop.table(tab_den)

        F         M         I 
0.1296296 0.2777778 0.5925926 

prop.table(tab_den)*100 # pour transformer en pourcentages 
round(prop.table(tab_den)*100,1) # et arrondir le résultat à une décimale
```

{% hint style = 'danger' %} N'oubliez pas d'alimenter votre script avec les commentaires {% endhint %}

![alimentation du script](images/script_1.png)



## 2.2. Représenter les variables qualitatives



### a. Le diagramme en bâton

La représentation par défaut d'une variable qualitative est le **diagramme en bâton**. D'ailleurs si nous utilisons la fonction par défaut de R pour faire une représentation graphique...

```R
plot(tab.den)
```


... nous pouvons constater que c'est le choix que le logiciel a fait !

### b. Le diagramme en barre

Le diagramme équivalent le plus couramment utilisé est le **diagramme en barre** accessible avec la fonction `barplot` 

```R
barplot(tab.den)
```

![diagramme en barre](images/barplot.png)

{% hint style = 'info' %} Attention, il ne faut pas confondre : 
le diagramme en barres :

   * Convient aux variables discrètes
   * Les barres sont disjointes car on veut différencier les modalités
  * C'est la hauteur de la barre qui est proportionnelle à l'effectif de chaque modalité

avec l'Histogramme :

  * convient aux variables continues
  * Pour exprimer la continuité les barres sont jointes
  * C'est la surface du rectangle (barre) qui est proportionnel à l'effectif de chaque classe. {% endhint%} 



L'objectif de la représentation graphique est de différencier l'effectif de chaque modalité ; il est possible d'ajouter la variable visuelle **couleur** afin de mettre en évidence cette différence. Nous utilisons alors l'argument *col =* (suivi d'une liste de couleurs) à l'intérieur de la fonction `barplot()`

```R
barplot(tab.den, col = c("gold", "coral", "darkturquoise"))
```

![barplot avec couleurs](images/barplot_color.png)

> Pour connaitre le nom des couleurs installées par défaut avec R vous pouvez consulter le mémo pdf [color in r](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwiaifnPovjiAhUHTcAKHTLhD_UQFjAAegQIBhAC&url=http%3A%2F%2Fwww.stat.columbia.edu%2F~tzheng%2Ffiles%2FRcolor.pdf&usg=AOvVaw1XIn-mwZ73RgG8bxh4Lan4) 

Un titre au graphique permettrait d'obtenir un résultat plus convenable... nous allons donc ajouter l'argument *main =* (suivi du texte du titre)

```R
barplot(tab.den, col = c("gold", "coral", "darkturquoise"), main = "effectifs selon \n le sexe")
```

<u>Note</u>: Notez l'usage de **\n** pour forcer un retour à la ligne ↑

![diagramme en barre avec couleurs](images/barplot_color_2.png)

Pour ajouter les titres des axes x et y, il faut utiliser les commandes `xlab`  et `ylab` :

```R
barplot(tab.den, 
        col = c("gold", "coral", "darkturquoise"), 
        main = "effectifs selon \n le sexe",
        xlab = "sexe des individus", 
        ylab = "effectif")
```



### c. Le diagramme à secteur circulaire

Pour représenter non plus l'effectif par modalités mais la proportion des modalités les unes par rapport aux autres, le **diagramme à secteurs circulaires** ou **camembert** est d'usage courant. Nous utilisons alors la fonction dédiée `pie()`

```R
pie(tab.den)
```

![camembert](images/pie_1.png)

Pour améliorer le graphique et explorer les arguments que l'on peut utiliser, il suffit de demander de l'aide à R en précédant le nom de la fonction d'un **?** ou bien la fonction `help()`

```R
?pie
help(pie)
```

La rubrique **Usage** décrit la fonction en montrant les arguments les plus courants que l'on peut utiliser.

```R
pie(x, labels = names(x), edges = 200, radius = 0.8,
clockwise = FALSE, init.angle = if(clockwise) 90 else 0,
density = NULL, angle = 45, col = NULL, border = NULL,
lty = NULL, main = NULL, ...)
```

> *Important :* les valeurs prises par les arguments dans l'exemple sont celles prises par défaut c'est à dire celles qui sont activées si on utilise la fonction sans ajouter d'argument.

Ainsi on peut faire un camembert un peu plus "léché" :

```R
pie(tab.den, clockwise = TRUE, border = FALSE, col = c("gold", "coral", "darkturquoise"), main = "proportion des individus par sexe")
```

Cette liste peut se lire de cette manière : 

```R
pie(tab.den 
    ,clockwise = TRUE # dans le sens des aiguilles d'une montre
    ,border = FALSE # pas de bordure
    ,col = c("gold", "coral", "darkturquoise") # définir les couleurs
    ,main = "proportion des individus par sexe" # définir le titre
    )
```

![camembert léché](images/pie_2.png)

> On ne va pas non plus en faire tout un fromage ;)
>

### d. Le diagramme en barres empilées

Il existe un autre graphique pour représenter la proportion des différentes modalités d'une variable discrète : la diagramme en barres empilées.

```R
barplot(as.matrix(tab.den) # il faut transposer la table avec as.matrix comme "série de données en ligne" dans Calc
        , col = c("gold", "coral", "darkturquoise") # définir les couleurs
        , legend.text = TRUE # mettre la légende
        )
```

![plot](images/barplot_3.png)

{% hint style = 'working' %} Exercice : à partir du fichier 'looberhe_structures.csv', faire un tableau de dénombrement du type de vestiges et générer une représentation graphique appropriée. {% endhint %}

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

# 3. Les variables quantitatives (ou continues)

## 3.1. Décrire les variables quantitatives

Pour cette partie, nous allons utiliser la variable $stature de notre tableau "bavay_stature_sexe". Reprenons la structure du tableau avec la fonction `str()`

```R
str(tableau)
```

```
'data.frame':	54 obs. of  3 variables:
 $ fait   : int  2069 2070 2071 2072 2087 2131 2206 2207 2261 2384 ...
 $ sexe   : chr  "I" "I" "I" "I" ...
 $ stature: num  161 NA NA NA 148 ...
```

Nous pouvons identifier la variable qualitative (de type num) à étudier : **$stature**

Pour un usage plus facile, nous lui affectons un nom d'objet plus court : 

```R
taille <- tableau$stature
```

{% hint style = 'tip' %} Lorsque vous saisissez le nom du tableau suivi du signe `$`pour indiquer la variable, la liste des variables du tableau apparaît. 

![écriture intuitive variable](images/ecriture_intuitive_variable.png)

{% endhint %}

### a. Les caractéristiques de tendance centrale

On cherche ici des nombres qui vont résumer le centre de la distribution.

#### La moyenne

```R
mean(taille)
```

```
[1] NA
```

Rappel : certaines fonctions n'acceptent pas la présence de valeurs manquantes, il faut alors ajouter l'argument `na.rm = TRUE` que l'on peut aussi écrire `na.rm = T`

```R
mean(taille, na.rm=T)
[1] 164.4884
```

La moyenne c'est la **somme des valeurs divisées par l'effectif total**. C'est la caractéristique de tendance centrale la plus commune. Attention, elle est sensible aux valeurs extrêmes.

#### La médiane

```R
 median(taille, na.rm=T)
[1] 163.8
```

La médiane est la **valeur, observée ou calculée, qui sépare la série statistique en 2** : 50 % des individus ont des modalités inférieure à la médiane et 50 % une valeur supérieure.

### b. Les caractéristiques de dispersion

#### L'étendue

```R
range(taille, na.rm=T)
[1] 147.6 178.0
```

```R
 max(taille, na.rm=T)-min(taille, na.rm=T)
[1] 30.4
```



#### L'écart-type

```R
sd(taille, na.rm=T)
[1] 6.513291
```

L'écart-type est la **caractéristique de dispersion autour de la moyenne**. Il s'exprime dans la **même unité que la variable**. Plus il est petit plus la série statistique est homogène.

#### Les quartiles

```R
quantile(taille, na.rm=T)
   0%   25%   50%   75%  100% 
147.6 160.2 163.8 169.7 178.0 
```

Les quartiles font partie de la famille des quantiles. A l'instar de la médiane qui divise une série statistique en 2, **les quartiles divisent cette série en 4**.

Ils sont utilisés pour construire une boîte à moustache.

> Notez que la fonction renvoie successivement : le minimum, le 1er quartile Q1, la médiane Q2, le 3ème quartile Q3 et le maximum.

#### L'intervalle inter-quartile

```R
IQR(taille, na.rm=T)
[1] 9.5
```



L'intervalle interquartile est **la différence entre Q3 et Q1**, autrement dit **c'est l'équivalent de l'étendue mais pour les 50% d'individus au centre de la distribution**.



### c. Résumé statistique

Il est possible d'obtenir directement un résumé statistique de la variable :

```R
summary(taille)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
  147.6   160.2   163.8   164.5   169.7   178.0      11 
```



{% hint style = 'working' %} Exercice : dans le tableau Haspres_mobilier.csv, calculer la moyenne et la médiane des Nombres de restes ; pour les colonnes indiquant le poids, fournir les caractéristiques de dispersion. {% endhint %}

## 3.2. Représenter les variables quantitatives

Le résumé numérique permet d'appréhender la distribution de notre variable quantitative, il est néanmoins nécessaire de l'accompagner d'une ou de plusieurs représentations graphiques. Analyser une représentation graphique d'une variable continue, c'est :

  - définir la forme de la distribution (normale, pluri-modale, assymétrique,...)
  - repérer les zones de concentration
  - repérer les zones de dispersion
  - identifier les ruptures dans la distribution

### a. Le scalogramme

C'est la représentation graphique la plus simple et la plus facile à lire. Chaque individu est reporté sur un graphique unidimensionel (un seul axe : celui des abscisses avec les valeurs de la variable continue). Si plusieurs individus ont la même valeur, on les superpose.

```
stripchart(taille)
```

On ajoute des éléments pour empiler les points avec `method = stack` et changer la forme des points `pch = 19 `


```R
stripchart(taille,
           method = "stack",
           pch = 19)
```

![scalogramme](images/scalogramme.png)

Sur cette représentation on peut facilement repérer les concentrations (beaucoup de points), les zones de dispersion (points isolés et éloignés les uns des autres) et les ruptures (les vides). 

Note : Si la distribution est trop asymétrique, préferez l'argument `method = "jitter"`



Par défaut, les limites du graphique sont calculées en fonction des valeurs. On peut souhaiter élargir ces limites, cela se fait au moyen des commandes `xlim` et `ylim` qui attendent les valeurs sous la forme d'un vecteur. Par exemple, `c(-10, 20)` indique que les limites doivent être comprises entre -10 et 20. 

On obtient, à partir des mêmes données, le résultat suivant en fixant les limites de l'axe des abcisses à 135 et 190 :

```R
stripchart(taille, 
             method = "stack", 
             pch = 19, 
             xlim = c(135, 190))
```

![scalogramme 2](images/stripchart_2.png)

![types de points](images/types_points_R.png)

### b. Le diagramme en tige et feuille

C'est une représentation qui liste toutes les modalités prises par les individus dans un graphique, elle se réalise avec la fonction `stem()`

```R
 stem(taille)

  The decimal point is 1 digit(s) to the right of the |

  14 | 8
  15 | 4
  15 | 556689
  16 | 000011122223344
  16 | 566889999
  17 | 011111334
  17 | 58
```

Cette représentation - peu utilisée - est une sorte d'histogramme avec des classes d'amplitude égales.

### c. la courbe des fréquences cumulées

Il faut au préalable construire un tableau avec 3 colonnes:
- la **variable quantitative triée** en ordre croissant
```R
tri <- sort(taille)
```
- la **fréquence relative** de chaque individu c'est à dire ce que représente la valeur de chaque individu par rapport à la somme totale des valeurs (en%).
```R
freq_rel <- tri/sum(tri)*100
```
- la **fréquence cumulée ascendante** c'est à dire la somme cumulée des fréquences relatives (le dernier individu représentera 100% ).
```R
freq_cum <- cumsum(freq_rel)
```

On peut maintenant tracer la **courbe des fréquences cumulées ascendantes** à l'aide de la fonction `plot(x,y)` avec la variable triée en abcisse et les fréquences cumulées en ordonnées.

```R
plot(x = tri, y = freq_cum
     , pch = 19
     , col = "blue"
     , type = "o" # = "overlapped" permet de superposer une ligne et des points
    )
```
![diag_freq_cum](images/diag_freq_cum.png)

{% hint style='tip' %}

 Il est possible de rassembler les 3 vecteurs dans un tableau de 3 colonnes avec la fonction `cbind`

```R
tab_freqcum <- cbind("stature (en cm)" = tri
					 , "fréquence relative" = freq_rel
                     , "fréquence cumulée ascendante" = freq_cum)
```

<u>Note 1</u>: remarquez la manière de renommer les colonnes ↑

<u>Note 2</u>: il est possible de sauvegarder ce tableau en .csv →  cf. le paragraphe 5

{% endhint %}



### d. L'histogramme

```R
 hist(taille)
```

![histogramme](images/hist.png)

On peut agrémenter l'histogramme :

- définir le nombre de classes a approcher.
- ajouter les étiquettes
- changer la couleur
- ajouter un titre

```R
hist(taille, 
     breaks = 5, 
     labels = T, 
     col = "navyblue",
     main = "distribution de la taille")
```

![histogramme amélioré](images/hist_2.png)

{% hint style='tip' %}
Pour paramétrer un histogramme à partir d'un fichier au format .csv (nombre de barres, méthodes de découpage des classes) il est possible d'utiliser l'application **Discretisator** (générée avec Rstudio) en ligne → https://archeomatic.shinyapps.io/Discretisator/
{% endhint %}

### d. La boîte à moustache

C'est une représentation graphique unidimensionnelle basée sur les quartiles, on l'obtient avec la fonction `boxplot()`

```R
boxplot(taille, horizontal=TRUE)
```

![boîte à moustache](images/boxplot_1.png)

On reporte sur l'axe des abscisses :

 - La médiane (Q2)
 - Les 1er et 3ème quartiles (Q1 et Q2)
 - On trace une boîte entre Q1 et Q2 qui représente les 50 % d'individus au centre de la distribution
 - Soit :
   - directement les valeurs mini et maxi
   - les valeurs aberrantes sous forme de points et les 1ères valeurs faibles non aberrante ainsi que la dernière valeur forte non-aberrantes.



Pour aller plus loin : 

On pourrait faire cette boîte à moustaches en fonction des modalités de la variable sexe : 

```R
boxplot(taille~tableau$sexe
        , col = c("coral", "gold", "darkturquoise")
        , horizontal = T)
```

![boxplot 2](images/boxplot_2.png)

et en ajoutant des points avec la moyenne

```R
points(mean(tableau$stature [tableau$sexe == "F"], na.rm=T) # coordonnées en X
       , 1 # coordonnées en Y
       , col = "red" # couleur
       , pch = 19 # forme du point
       )
points(mean(tableau$stature [tableau$sexe == "I"], na.rm=T) # coordonnées en X
       , 2 # coordonnées en Y
       , col = "red" # couleur
       , pch = 19 # forme du point
)
points(mean(tableau$stature [tableau$sexe == "M"], na.rm=T) # coordonnées en X
       , 3 # coordonnées en Y
       , col = "red" # couleur
       , pch = 19 # forme du point
)
```

![boxplot selon la variable qualitative $sexe](images/boxplot_3.png)

### e. Superposer deux graphiques

Il est possible de superposer deux graphiques avec la fonction `par(new = T)` .

Pour cela nous allons utiliser d'autres données : 

```R
moutons <- read.csv2("C:/initiation_R/donnees/moutons.csv")
```

Nous allons superposer un scalogramme sur une boîte à moustaches : 

```R
boxplot(moutons$long_total, horizontal = T, col="cadetblue3")
par(new=T)
stripchart(moutons$long_total,
           method="jitter",
           pch=19,
           col="darkslategrey")
```

![superposition de graphiques](images/superposition.png)



## 3.3. Agrémenter un graphique

Les éléments qui vont autour d'un graphique ne relèvent pas seulement de l'esthétique, il s'agit de répondre aux règles de la sémiologie graphique. Tout ces ajouts permettent d'aller dans le sens d'une figure lisible et aisément compréhensible. 

A partir du tableau de dénombrement initial 'tab.den', nous allons ajouter différents éléments.

#### Le titre du graphique

Au moyen de la fonction `main` suivie d'un texte entre guillemets ; de la même manière, on ajoutera un sous-titre avec `sub` 

```R
barplot(tab.den, main = "Effectifs selon le sexe")
```



#### Changer le type de police

Pour la graisse, il faut indiquer un nombre entier : 

```R
barplot(tab.den, font = 2)
```

> 1 = normal (par défaut)
>
> 2 = gras
>
> 3 = italique
>
> 4 = gras et italique



#### Le titre des axes

Au moyen de `xlab` et `ylab` :

```R
boxplot(taille, 
	horizontal=TRUE, 
	main = "Dispersion de la stature des \n individus adultes",
	xlab = "stature en cm")
```

L'orientation des axes peut être modifiée avec `las` :

> las = 0 : toujours parallèle à l'axe, par défaut
>
> las = 1 : toujours horizontal
>
> las = 2 : toujours perpendiculaire à l'axe
>
> las = 3 : toujours vertical

​	

#### Ajouter une ligne

On peut ajouter une ligne repère, avec la commande `abline`, de type :

```R
abline(h = y) # pour tracer une ligne horizontale de coordonnées y
abline(v = x) # pour tracer une ligne verticale de coordonnées x
```

Par exemple :

```R
abline(v = mean(taille, na.rm=T), col = "red")
```

#### Ajouter une grille de fond

Avec la commande `grid`, en utilisant les paramètres de taille et de type de trait : 

```R
barplot(tab.den, grid(lty=6))
```



#### La légende

```R
legend.text = TRUE
```

Elle ne fonctionne pas dans tous les cas de figure.



#### Les couleurs

```R
boxplot(taille, 
         horizontal=TRUE, 
         main = "Dispersion de la stature des \n individus adultes",
         xlab = "stature en cm", 
         col = c("gold"))
```

Il est possible aussi d'utiliser des fonctions générant des couleurs ; les deux suivantes avec *n* en nombre entier : 

```R
col = rainbow(n)
col = heat.colors(n)
```

Ou bien encore les codes hexadécimaux des couleurs :

```R
col = "#120019"
```



#### Changer le type ou la largeur des traits

Pour changer la largeur du trait, utiliser le paramètre `lwd` pour *line width*, suivi d'un nombre entier :

```R
boxplot(taille, horizontal = TRUE, lwd = 5)
```

Pour changer le type de trait, utiliser le paramètre `lty` pour *line type* :

```
lty = 1 
```

0 = blanc, 1 = uni (par défaut), 2 = tirets, 3 = pointillés , 4 = point-tiret, 5 = tiret long, 6 = deux tirets

#### Changer la largeur d'une barre

```R
barplot(tab.den, xlim = c(0, 10), width = 0.8)
```

Attention, le paramètre `width` n'est utilisable que si `xlim` a été fixé :

![paramètre de largeur](images/width.png)

#### Changer l'espacement entre les barres

```R
barplot(tab.den, xlim = c(0, 10), width = 0.8, space = 2)
```

Par exemple ici, l'espacement sera de 2 fois la largeur moyenne des barres

![espacement des barres](images/space.png)



## 3.4. Exporter les graphiques

L'export se fait via la fenêtre *Plots*, en général située à droite. Cliquer sur *Export* puis choisir le format : 

![export graphique](images/export_image_1.png)

En cas d'export sous forme d'image, dans la fenêtre qui s'ouvre, on peut choisir le format (png par défaut), le dossier *Directory* et le nom *File name* ("Rplot" par défaut). A droite, on peut choisir les dimensions en pixels de l'image et en cochant la case *Maintain aspect ratio*, cela conserve les proportions si vous modifiez l'une ou l'autre des dimensions. 

![export au format image](images/export_image_2.png)

En cas d'export en pdf, il faut choisir les dimensions dans le menu déroulant, puis l'orientation et enfin le dossier et le nom du fichier. 

![export pdf](images/export_image_3.png)


{% hint style='working' %} A partir de bavay_complet.csv :

- nouveau script, importer le fichier ; 
- identifier deux variables qualitatives discrètes, faire un tableau de dénombrement et un tableau de dénombrement en fréquence, choisir une représentation graphique adaptée pour chacune de ces deux variables ; 
- identifier et choisir deux variables quantitatives continues, décrire les caractéristiques de tendance centrale et les caractéristiques de dispersion, terminer par un résumé statistique, choisir une représentation graphique adaptée pour chacune de ces deux variables
{% endhint %}

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

# 4. Manipuler les données dans R

Il arrive qu'on ait besoin de travailler que sur une partie des données ; pour cela, nous allons créer des objets ne regroupant qu'une partie des données. 

La fonction suivante `[,] ` permet d'extraire des données. La partie avant la virgule concerne les individus (= les lignes), la partie après la virgule concerne les variables (= les colonnes). Si on laisse vide l'une des parties, toutes les données seront sélectionnées.

Par exemple pour extraire les colonnes "fait", "long_cont", "large_cont", pour la totalité des individus :

```R
dimcont1 <- bavay[ ,c('fait', 'long_cont','larg_cont')]
View(dimcont1)
```

Il s'agit d'une copie intégrale de tous les enregistrements, en ne conservant que ces trois colonnes.

Dans le cas de notre requête, nous voulons cet extrait, mais seulement pour les individus inhumés en cercueil cloué. Dans ce cas, il faudra, avant la virgule de la section entre crochets, indiquer les individus à sélectionner :

```R
dimcont2 <- bavay[bavay$contenant == "cercueil cloué", c('fait', 'long_cont','larg_cont')]
View(dimcont2)
```

On note le signe == pour obtenir une égalité stricte.



Si on veut plusieurs variables, on utilisera l'opérateur `%in%` , suivi de l'opérateur de combinaison `c()` :

```R
dimcont3 <- bavay[bavay$contenant %in% c("cercueil cloué", "coffrage"), c('fait', 'long_cont','larg_cont')]
```

On peut alors faire une représentation graphique des longueurs et largeurs de contenants, pour les sépultures où un cercueil cloué ou un coffrage ont été identifiés :

```R
plot(dimcont3$larg_cont~dimcont3$long_cont
     , xlab = "longueur des contenants en cm"
     , ylab = "largeur des contenants en cm"
     , pch = 3
    )
```

La fonction pour ce graphique s'écrit `graphique(y~x)` avec le tilde qui signifie "en fonction de".

![graphique des dimensions de contenants](images/plot_dimensions.png)

On peut aussi avoir des résumés statistiques : 

```R
summary(dimcont3$long_cont)
```



Si l'on souhaite obtenir une sélection des individus sexés avec certitude : 

```R
HF <- bavay[bavay$sexe %in% c("Masculin", "Féminin"), ]
View(HF)
```

L'opérateur `!=` permet d'exclure une valeur : par exemple pour ne sélectionner que des individus pour lesquels le sexe est déterminable : 

```R
HF2 <- bavay[bavay$sexe !="Immature", ]
View(HF2)
```



{% hint style='working' %}
Créer un tableau avec 4 colonnes ('fait', 'long_cont', 'larg_cont', 'clous'), en ne sélectionnant que les sépultures avec un contenant en cercueil cloué ou cercueil cloué et traverses, et pour lesquelles le nombre de clous est supérieur ou égal à 3.
{% endhint %}

Réponse : 

```R
exo <- bavay[bavay$contenant %in% c("cercueil cloué", "cercueil cloué et traverses") 
             & bavay$clous >= 3,
             c('fait', 'long_cont', 'larg_cont', 'clous')]
View(exo)
```

On peut obtenir un tableau avec le dénombrement des sépultures en fonction du nombre de clous : 

```R
table(exo$clous)
```

Et aussi un résumé statistique :

```R
summary(exo$clous)
```



# 5. Exporter un tableau de données

On peut utiliser la fonction  `write_csv2` : 

```R
write.csv2(dimcont3, "contenant_cloues.csv")
```

On obtient alors un fichier csv utilisable ailleurs.

<div style="page-break-after: always;"></div>

# Ressources

Rarement dédiées à l'archéologie, il existe de nombreuses ressources en ligne pour se former aux statistiques et à l'analyse de données:

- pour une découverte simple et succincte : le [site Begin'R](https://beginr.u-bordeaux.fr/beginr.html) de l'université de Bordeaux.

* l'indispensable[ Introduction à R et au *tidyverse*](https://juba.github.io/tidyverse/) de Julien Barnier. L'équivalent en ligne de 336 pages claires et concises ! très complet pour commencer avec R, Rstudio, les stats uni et bi-variées. Ce document est mis à jour régulièrement et peut être téléchargé en pdf. 

* Le site des [formations à R](https://mtes-mct.github.io/parcours-r/) du ministère de la Transition Écologique et Solidaire, en particulier le module "[Préparation des données](https://mtes-mct.github.io/parcours_r_socle_preparation_des_donnees/)"

- Dans la même veine et pour compléter : [Analyse-R - Introduction  à l’analyse d’enquêtes  avec R et RStudio](http://larmarange.github.io/analyse-R/). L'équivalent de 1092 pages d'un cours en ligne divisé en 3 rubriques : Manipuler, Analyser et Approfondir. Version téléchargeable en PDF pour les papierophiles.

- [R pour statophobes](https://perso.univ-rennes1.fr/denis.poinsot/Statistiques_ pour_statophobes/R pour les statophobes.pdf) (PDF) de Denis Poinsot (l'auteur du célèbre [Statistiques pour statophobes](https://perso.univ-rennes1.fr/denis.poinsot/Statistiques_ pour_statophobes/STATISTIQUES POUR STATOPHOBES.pdf) (pdf) ou comment se marrer avec des stats). A peine 36 pages pour commencer avec R et faire des stats univariées et bi-variées.

- Pour des statistiques amusantes on n'oublie pas le [blog de Lise Vaudor](http://perso.ens-lyon.fr/lise.vaudor/), son [grimoire pour apprendre les stats](http://perso.ens-lyon.fr/lise.vaudor/grimoire/) avec R mais aussi des princesses :princess:  et des dragons :dragon:  et depuis peu sa chaîne [youtube](https://www.youtube.com/channel/UC9QOCBVGHp4gnmcmkG1VbGg).

- Plus près des archéologues, [Éléments de statistiques](http://www.pacea.u-bordeaux.fr/Cours-de-statistique-en-M1-AbP.html) le cours pour les M1 de Bordeaux par Frédéric Santos (PACEA).

- Pour finir, faites le formidable Mooc (Cours en Ligne) [Introduction à la statistique avec R](https://www.fun-mooc.fr/courses/UPSUD/42001S06/session06/about). 

  >  Attention:  Il est préférable de commencer à la date de lancement d'une session et compter plusieurs heures sur 5 semaines avec exercices (QCM et devoirs  corrigés -à corriger-  par ses pairs). Pédago exceptionnelle !  


https://mtes-mct.github.io/parcours_r_socle_preparation_des_donnees/

https://mtes-mct.github.io/parcours_r_module_datavisualisation/

https://sites.google.com/site/rgraphiques/home?authuser=0

https://r4ds.had.co.nz/



https://www.data-to-viz.com/



[^réponse1]:  `t_cercueil_clou <- select(sep, fait, contenant) %>% filter(contenant == 'cercueil cloué'| contenant == 'cercueil cloué')`