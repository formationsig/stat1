# module 1.1. Décrire, analyser et représenter ses données (avec un tableur)



### Les encarts

Dans ce déroulé vous trouverez 4 types d'encarts:

{% hint style='info' %}
Information: indique une information d'ordre général
{% endhint %}

{% hint style='danger' %}
Attention: indique une information importante ou un problème récurrent
{% endhint %}

{% hint style='tip' %}
Astuce: indique une astuce, un raccourci
{% endhint %}

{% hint style='working' %}
Exercice: indique des manipulations à faire ou l'énoncé d'un exercice
{% endhint %}

{% hint style='zob' %}
Autre: exemples, aparté, information facultative
{% endhint %}

{% hint style='zob' %}
{% endhint %}

{% hint style='zob' %}
exemple: `= MOYENNE(SOMME(A1:B3);SOMME(1;2;3;4))`
{% endhint %}





# Rappel de l'épisode précédent

pré-requis est "Les données archéologiques : préparer et consolider ses tableaux"

Dans l'épisode 0, vous avez appris à trier un tableau, à manipuler les cellules et à utiliser quelques fonctions de base.

Dans l'épisode actuel, maintenant que les données sont "propres", nous allons travailler sur les données archéologiques en commençant par les décrire, puis en les analysant et en les représentant, le tout dans un logiciel de tableur. 



## Le logiciel utilisé



Nous avons fait le choix d'utiliser le logiciel LibreOffice Calc, pour plusieurs raisons :

* Il est installé par défaut sur tous les ordinateurs Inrap.
* C'est un logiciel libre (tout le monde peut  l'installer gratuitement) et multi plateforme (Windows, Mac OS, Linux, etc).
* Il dispose de toutes les fonctions nécessaires à la préparation des données, à leur manipulation et à leur représentation graphique.
* Il possède les mêmes fonctionnalités qu'Excel et son interface minimaliste est proche des anciennes versions de celui-ci.
* Il peut importer et exporter tous les formats de fichiers courants (csv, dbf, xls, xlsx), et le format ods est compatible avec les autres tableurs.



# 1. Décrire ses données 

##  1.1. Quelques définitions

### Population

En statistique, on travaille toujours sur une population statistique.

**La population** est l’ensemble des individus sur lesquels des observations et des mesures sont réalisées.

***n*** **désigne le nombre d’individus d’une population en langage statistique**.

*Exemples* : les stagiaires de la formation stat, des céramiques, des tombes, des os, etc.

***Attention*** la population doit être définie clairement. Quelles sont les sources et les critères qui définissent cette population ?

 

{% hint style='zob' %} Les stagiaires de la formation stat ? Est-ce une définition de population suffisante ?

→ Qu’est-ce qui définit un stagiaire de la formation stat ? (ceux ici présents ? Tous ceux, des 1000ers qui ont suivis cette formation ? Ceux qui sont inscrits, même s'ils sont pas venus ?...) (Clarté de la population)

→ À quelle date la liste des stagiaires a-t-elle été établie ? (importance du temps dans la définition de la population){% endhint %}



Cela revient à bien définir son échantillon.

### Echantillon

C’est un sous-ensemble de la population, réellement accessible à l’expérimentateur. Dans le cas des études archéologiques cet échantillon est souvent très réduit.



### Individu

Cette population comprend un certain nombre d’individus ou d’unités statistiques (chaque fait, chaque céramique, pièces isolées….).

L’individu est une **entité élémentaire** sur laquelle on va mesurer ou observer des phénomènes.

Il correspond à une ligne et une seule dans le tableau. Il possède un identifiant unique (ID).

Une façon habituelle de procéder consiste à identifier chaque individu par un nombre entier compris entre 1 et n et que l'on désigne par : 
$$
i (i=1, ....., n)
$$
*i* désigne un individu quelconque en langage statistique

*Exemple : une tombe étudiée : la 312, le tesson de bord de la dressel 654 qui fait 2 cm d'épaisseur*



### Variable ou caractère

On appelle **caractère** ou **variable** une **propriété commune à tous les individus d’une population** statistique. Cette caractéristique ou propriété quelconque peut (ou non) différer d’un individu à l’autre.



On appelle variable une série de mesures ou d'observations effectuées sur un échantillon.

**Elle correspond à une colonne et une seule dans le tableau.**

*Exemple : si on étudie un ensemble de squelettes d’une nécropole, on peut les décrire par leur sexe, leur taille, leur position, etc.*

*Exemple : si on étudie un ensemble de silex, la nature pétrographique est un caractère que l’on peut observer, de même que la forme*

chacun des individus d’une population peut être considéré du point de vue de un ou plusieurs caractères.

{% hint style='zob' %}
Chaque variable (caractère) peut aussi être identifiée par un nombre entier k compris entre 1 et p
$$
(k = 1, ...., p)
$$
{% endhint %}

### Modalité

Une modalité est la valeur ou la situation prise par une variable pour un individu.

→ Un caractère présente plusieurs modalités.

→ Chaque individu statistique se caractérise par une modalité qui lui est propre.

La valeur observée sur l'individu i pour la variable k est représentée par la notation 
$$
x_i~k
$$


En statistique univariée, on note simplement Xi la modalité de la variable X enregistrée pour l’individu i.

Les modalités sont :

- **Incompatibles entre elles** : pour une variable X, un individu i ne peut enregistrer qu'une seule modalité Xi.
- **Exhaustives** : tous les individus d'une population possèdent une modalité pour la variable), sinon on parle de valeur manquante

{% hint style='zob' %} *Exemple* : Tous les squelettes étudiés d'une nécropole n'ont pas la même taille (mais la taille de chaque squelette doit avoir été estimée pour être incluse dans l'analyse). Chacune des valeurs de taille est une modalité de la variable «taille».
{% endhint %}



### Tableau élémentaire

Il s’agit d’un **tableau à double entrée** où l**es lignes correspondent aux individus** et **les colonnes aux variables** décrivant ces éléments.

La 1ère colonne est souvent réservée à la liste nominale des éléments sans que cela soit obligatoire. 

Chaque ligne de ce tableau correspond à un individu pour lequel on dispose de  ![img](C:\gitlab_inrap\stat1\mod1_1_description\images\p.png) valeurs observées. Chaque colonne correspond à une variable pour laquelle on a effectué  ![img](C:\gitlab_inrap\stat1\mod1_1_description\images\n.png) mesures. Le double indice ![img](C:\gitlab_inrap\stat1\mod1_1_description\images\ik.png) utilisé dans la notation ![xik](images/xik.png) nous indique directement dans quelle ligne et quelle colonne se situe cette donnée.

![tableau élémentaire](images/tableau_elem.png)



## 1.2. Les types de variables

On voit à travers les exemples précédents qu’il existe plusieurs types de caractères. Chaque variable étudiée renvoie à un phénomène archéologique bien particulier.

Il peut s’agir de :

- **phénomènes concrets** (effectifs d’une population, production agricole, type de spécialisation, parc automobile..) ou **abstraits** (flux d’informations, de capitaux…)

- **phénomènes quantifiables** (effectifs de population d'une nécropole, dimensions d'un silex…) ou **non quantifiables** (datation, orientation, type de fait…)

- **phénomènes fixes** (**entités** : taux d’urbanisation, effectifs…) ou **évolutifs** (**processus** : évolution d’effectifs, de taux, tendances…)

Lorsque l’on travaille sur des données l’une des premières choses à vérifier est le type de variable auquel on est confronté. A chaque type de variable correspond des outils statistiques différents et des règles sémiologiques précises.

{% hint style='danger' %}

On classe les variables en 2 catégories :  

- Les caractères **Qualitatifs** (QL ou q) ou facteur

Les modalités expriment l'appartenance à une catégorie.

- Les caractères **Quantitatifs** (QT ou Q)

Les modalités s'expriment en nombres réels sur lesquels les opérations arithmétiques courantes ont un sens. Il est possible de les ordonner et de faire des calculs dessus.{% endhint %}

### Les variables qualitatives

#### Qualitative ordinale

![variable qualitative ordinale](images/v_ql_ordinale.png)

On parle de qualitatif ordinal lorsque les modalités correspondent à des rangs (l’ordre des modalités à un sens, il possède une logique)
*Exemple : on classe la chronologie selon un sens temporel (Préhistoire / Protohistoire / Antiquité). On peut classer des os selon un degré de conservation (TB, B, moyen, mauvais)*
{% hint style='danger' %} Attention il peut être exprimé sous forme de nombre (genre 1-2-3-4) ce n'est pas pour autant un caractère quantitatif.{% endhint %}

#### Qualitative nominale

![variable qualitative nominale](images/v_ql_nominale.png)

On parle de qualitatif nominal lorsque l’ordre des modalités n’a pas de sens ; les modalités ne sont pas ordonnées entre elles.
*Exemple : orientation de sépultures, présence/absence de carie sur une dent, de négatif dans un poteau.*

#### Qualitative discrète

![variable qualitative discrète](images/v_ql_discret.png)

Un caractère qualitatif est discret lorsque le nombres de modalités est limité = Il y a moins de modalités que d'individus.
*Exemple : type de faits (généralement liste limitée : TP, Fosse,.. ou période chronologique.*

#### Qualitative exhaustive

![variable qualitative exhaustive](images/v_ql_exhaustif.png)

Un caractère qualitatif est exhaustif lorsqu'il y a autant de modalités que d'individus.
*Exemple : difficile à trouver à part les ID genre le numéro de fait ?*

### 

### Les variables quantitatives

#### Quantitative absolue

![variable quantitative absolue](images/v_qt_absolue.png)

On parle de quantitatif de absolu (ou de stock) lorsque nous avons affaire à des nombres réels continus
*Exemple : une surface, une taille*

Un caractère quantitatif absolu exprime des quantités concrètes = la somme des modalités des individus a un sens.
*Exemple : NR-NMI, diamètre, longueur, poids…*

#### Quantitative relative

![variable quantitative relative](images/v_qt_relative.png)

On parle de quantitatif relatif (*ou* d’intensité *ou* de taux *ou* de rapport) lorsque nous avons à faire à un rapport entre deux valeurs.
*Exemple : nombre de silex par m², taux de fragmentation en %*
On peut calculer une moyenne mais la somme n’a pas de sens.
*Exemple : on peut faire une moyenne des densité de silex par carré mais pas les additionner : 10 silex/m² dans un carré + 20 silex/m² dans un autre carré, ça fait pas 30 silex au m² !!!!*
Les modalités peuvent être ordonnées. On peut classer la densité de silex ou les taux de fragmentation.

#### Quantitative discrète

![variable quantitative discrète](images/v_qt_discrete.png)

On parle de quantitatif discret quand les valeurs mesurées correspondent uniquement à un nombre fini de valeurs isolées (généralement des nombres entiers, mais pas forcément).
*Exemple : nombre d'agents Inrap, nombre de structure dans une emprise, NR-NMI, nombre de ville de plus de 100000 h dans chaque département*

#### Quantitative continue

![variable quantitative continue](images/v_qt_continue.png)

La notion de quantitatif continu fait référence à l’échelle numérique des valeurs qui est celle des nombres réels. Les valeurs potentiellement prises par la variable sont en nombre infini.
*Exemple : longueur d'un tibia, un diamètre, une taille, ...*
La moyenne des valeurs a alors un sens.

#### Quantitative mesurable

![variable quantitative mesurable](images/v_qt_mesurable.png)

On parle de quantitatif mesurable quand on peut mesurer une modalité sur une échelle numérique. Le 0 signifie bien l'absence du phénomène
*Exemple : population, taux de fécondité, précipitations*

#### Quantitative repérable

![variable quantitative repérable](images/v_qt_reperable.png)

On parle de quantitatif repérable quand une modalité est repérable sur une échelle d'intervalle.
Ces caractères permettent de repérer la position de chaque élément par rapport à une origine arbitraire. La valeur 0 est donc conventionnelle et ne signifie pas l'absence du phénomène.
*Exemple : Latitude, longitude, température, altitude, …*





![tableau récapitulatif des types de données](C:\gitlab_inrap\stat1\mod1_1_description\images\recap_type_donnee.png)



# 2. Analyser et représenter les variables qualitatives

### Tableau de dénombrement

Le tableau de dénombrement est un tableau élaboré de construction de l'information. Il est construit à partir du tableau élémentaire. A chaque modalité de la variable correspond un certain nombre d’unités statistiques appelées effectif de la modalité.

Il y a en général autant de tableaux de dénombrement qu’il y a de variables.

Le tableau de dénombrement se compose de trois colonnes : 

|       | Fréquence absolue | Fréquence simple   |
| ----- | ----------------- | ------------------ |
| Type  | Effectif          | Fréquence relative |
| Fosse | 114               | 27.74              |
| Silo  | 128               | 31.14              |
| TP    | 169               | 41.12              |
|       | 411               | 100                |

- **Première colonne** : la liste des modalités du caractère ![](images/Xmaj.png), ou bien la suite des classes de valeur du caractère si les modalités numériques ont été regroupées en classes
- **Deuxième colonne** : l’effectif de la modalité ![](images/Xi.png), c'est-à-dire le nombre d’éléments qui prennent cette modalité dans la distribution observée. La somme des effectifs pris pour chacune des modalités donne la population ![](images/Nmaj.png)(appelé aussi Fréquence Absolue)
- **Troisième colonne** : le rapport de l'effectif de cette modalité à l'effectif total,appelé la Fréquence simple ou Fréquence relative d'une modalité ![](images/Xi.png). La fréquence varie de 0 à 1, elle est alors notée sous forme décimale dans [0;1]. Elle peut être exprimée en pourcentage, elle varie alors de 0 % à 100 %.La somme des fréquences simples est égale à 1 (ou à 100 %) des éléments.

### Tableau croisé dynamique

On peut réaliser le tableau de dénombrement "à la main", mais il est aussi possible de le générer rapidement à partir d'un tableau croisé dynamique (:bulb: fiche technique)



### Diagramme en bâton

C'est la représentation graphique normale d'un caractère discret.
Il présente en abscisse la suite ordonnée des modalités du caractère et en ordonnée leur fréquence simple ou leur effectif.
**Les bâtons ne doivent pas être jointifs car le caractère est discret.**

Ils font apparaître des rectangles de base constante (ou mieux des bâtons/Lignes), dont les hauteurs sont proportionnelles aux effectifs (Fréquence Absolue) ou à la Fréquence (Fréquence Relative).

### Diagramme en secteur circulaire

Représentation équivalente au diagramme en bâtons mais moins performante sur le plan visuel. 

{% hint style='danger' %}
Bien qu’elle soit assez souvent utilisée, cette représentation est à exclure de vos pratiques. D'autant plus si vous voulez comparer plusieurs variables.{% endhint %}
Ils permettent de visualiser des parts relatives, dans des surfaces ou secteurs de cercle, que l’on différencie par des couleurs ou des trames différentes.
Chaque secteur correspond à une modalité.

### Barre coupée / Diagramme empilé

Représentation consistant à découper une barre (représentant 100% de l’effectif) en segments dont la longueur est proportionnelle à l’effectif de chaque modalité.

Particulièrement intéressante dans le cas des caractères où il existe un ordre entre les modalités.

### Diagramme en étoile

Représentation consistant à faire partir d’un point central autant de traits qu’il y a de modalités et à leur donner une longueur proportionnelle à leur fréquence.

Cette représentation est particulièrement adaptée au cas des caractères cycliques tels que les jours de la semaine, les mois de l’année, etc.



## Variable quantitative discrète

Il s’agit d’un **cas intermédiaire entre les variables continues et les variables qualitatives.**

 

D’une certaine manière, **si le nombre de valeurs prises par la variable est faible, cela s’apparente à une variable qualitative ordonnée**, et on effectue les mêmes représentations et descriptions qu’en section précédente.

Néanmoins, comme dans le cas des variables continues, **les notions de moyenne et d’écart-type gardent un sens et complètent le tableau**.



# 3. Analyser et représenter les variables quantitatives

### Tableau de distribution

Dans le cadre des caractères continus, il est nécessaire de réaliser un tableau de distribution.

Il s’agit d’**un tableau élémentaire dans lequel les valeurs du caractère X ont été ordonnées en ordre croissant.**

Le tableau de distribution statistique est donc l'ensemble ordonné (par ordre croissant) des valeurs prises par un caractère quantitatif.

**Le tableau de distribution statistique est donc un simple reclassement du tableau élémentaire.**

### Représentation numérique

Une série de données peut être résumée par quelques valeurs numériques appelées caractéristiques des séries statistiques, classées en 4 classes :

- ​	les caractéristiques de tendance centrale (moyenne, médiane,...)  	 
- ​	les caractéristiques de dispersion (écart-type, variance,...)
- ​	les caractéristiques de formes (normale, unimodale, bimodale,...)
- ​	les caractéristiques de concentration


 Nous en avons tous déjà entendu parler et elles nous sont plus ou moins familières...



#### Caractéristiques de position (ou de tendance centrale)

Elles donnent une idée de l'ordre de grandeur des valeurs constituant la série ainsi que la position ou semblent se concentrer les valeurs de cette série. Les principales caractéristiques de tendance centrale sont la moyenne arithmétique, la médiane, la médiale, le mode et les quantiles.

##### La moyenne arithmétique

Pour rester simple la moyenne arithmétique c'est la somme de toutes les valeurs observées divisée nombre d’observations

Dans Calc :

```
=MOYENNE(plage)
```

Propriétés :

- La moyenne arithmétique permet de résumer par un seul nombre la série statistique.
- Elle prend en compte toutes les valeurs de la série et elle est facile à calculer.
- Elle est sensible aux valeurs extrêmes, il est parfois nécessaire de supprimer des valeurs extrêmes ou « aberrantes ».
  Le chatisticien : “Quand Bill Gates rentre dans un bar, en moyenne tout le monde est millionaire!” 

On peut ici faire une pause “écriture formelle mathématique” de  la moyenne en décomposant la formule.

{% hint style='zob' %}**Le langage mathématique**, s’il peut paraître abscons, a un intérêt : c’est un langage universel qui permet de généraliser une formule en loi.

Essayons avec la **Moyenne** !

1. Soit  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_6ce721c38d7d6079.png) un individu,  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_7a39663d78e189b4.png) la variable continue et  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_58a3000745071658.png) l’effectif

2. La Moyenne c’est “la **somme de toutes les valeurs observées divisée par le nombre d’observations**”

3. On part de la somme qui, en mathématique s’écrit avec un *sigma* majuscule :  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_8c23f4f1e91c2eae.png)



 Par contre il faut lui indiquer “la somme de quoi ?”. La somme des modalités :  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_b45441b9047bf520.png)

 

5. Oui mais quelles modalités ? Pour celles qui correspondent à l’individu de rang 1:![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_ca8ddcb547012ec5.png)

6. jusqu’au  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_efdc8dc57d2bbec0.png) ième individu:![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_34c8d93c4f76304d.png) 

7. reste plus qu’à diviser cette somme par le nombre d’observations (=l’effectif):![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_a5d024a3fc74a3cd.png) 

8. Et voilà ! à ceci près qu’en math divisé par  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_b8ccc3a23ba5c7a6.png) revient à multiplier par![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_5be143e06988fbcc.png) : ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_6d864856308745e.png)



9. … et que par convention on peut se passer des bornes  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv57_tmp_f9ff3a2b232e3f5f.png)

10. C’est pas si dur !{% endhint %}



##### La médiane (Me)

La médiane est la valeur, observée ou possible, dans la série de données classée par ordre croissant (ou décroissant) qui partage cette série en deux parties comprenant exactement le même nombre de données de part et d'autre de Me.

Son calcul a pour but de décomposer la distribution statistique afin qu’il y ait le même nombre d’observations « avant » et « après» la médiane (soit 50 % de l’effectif avant et 50 % après). Pour ce faire il faut :

(1) ordonner les valeurs

(2) déterminer la médiane

Dans Calc :

```
=MEDIANE(plage)
```

Propriétés :
    • Le calcul de la médiane est rapide.
    • La médiane n'est pas influencée par les valeurs extrêmes ou aberrantes.
    • La médiane est influencée par le nombre d'individus (de données) mais pas par leurs valeurs.
    • Si la variable statistique est discrète, la médiane peut ne pas exister ; elle correspond seulement à une valeur possible de cette variable.
    • La médiane est le point d'intersection des courbes cumulatives croissantes et décroissantes.
    • La médiane ne se prête pas aux combinaisons arithmétiques ; la médiane d'une série ne peut pas être déduite des médianes des séries composantes.

##### Le mode (Mo)

Le mode, c'est la **valeur la plus souvent observée** dans un ensemble de données (aucun à plusieurs modes sont possibles).

Le Mode est la valeur de la variable statistique la plus fréquente que l'on observe dans une série d'observations.
Si la variable est une variable discrète, le Mode s'obtient facilement. Si la variable est une variable continue, on définit une classe modale.

Dans Calc :

```
=MODE(plage)
```

Propriétés :

- Le Mode n'existe pas toujours et quand il existe, il n'est pas toujours unique.
- Si après regroupement des données en classes, on trouve deux ou plusieurs modes différents, on doit considérer que l'on est en présence de 2 ou plusieurs populations distinctes ayant chacune leurs caractéristiques propres ; dans ce cas, la moyenne arithmétique n'est pas une caractéristique de tendance centrale !

#### Caractéristiques de dispersion

Ces caractéristiques quantifient les fluctuations des valeurs observées autour des valeurs observées autour de la valeur centrale et permettent d'apprécier l'étalement de la série. Les principales sont : l'**écart-type** ou son carré la **variance**, le **coefficient de variation** et l'**étendue**.

##### L'étendue

C'est la différence entre la valeur maximum et la valeur minimale.

Propriétés :

- L'étendue est facile à calculer.
- Elle ne tient compte que des valeurs extrêmes de la série ; elle ne dépend ni du nombre, ni des valeurs intermédiaires ; elle est très peu utilisée dès que le nombre de données dépasse 10.
- Elle est utilisée lorsque le nombre d'individus est de 4 ou 5 ; elle donne alors une idée appréciable de la dispersion. Cependant, dès que le nombre d'individus est de 15 à 20, il est préférable d'utiliser l'écart-type pour apprécier la dispersion.

Dans Calc :

```
=MAX(plage)-MIN(plage)
```

##### La variance et l'écart-type

La **variance** d'un échantillon, noté *s²* ou *V*, est appelée aussi **écart quadratique moyen** ou **variance empirique**. La racine carrée de la variance est appelé **écart-type**. Il s’exprime dans l’unité.

L'Écart-type est la mesure de dispersion la plus couramment utilisée en statistique lorsque l'on utilise la moyenne pour calculer une tendance centrale.
L'Écart-type mesure donc la dispersion autour de la moyenne.
Il est très utile pour comparer 2 séries statistiques qui ont approximativement la même moyenne.

Comme la moyenne arithmétique x, l'écart-type s s’exprime avec la même unité que les valeurs observées. On peut donc présenter une série statistique ainsi : moyenne +/- écart-type

{% hint style='info' %}
Plus l’écart-type est petit plus les données sont regroupées autour de la moyenne.
Il n'est pas toujours facile d'évaluer l'importance que doit avoir l'écart-type pour que les données soient largement dispersées. Il faut penser à l'unité de la variable.
Par exemple : Si on compare les recettes de 2 entreprises, un écart de 10000 ($) est peu significatif alors que pour des tailles de 2 populations, 50 (cm) c'est énorme ! {% endhint %}Remarques :

*Les propriétés de l’Écart-type :*

- On utilise l'écart-type uniquement pour mesurer la dispersion autour de la moyenne
- L'écart-type n'est jamais négatif (basé sur un carré !)
- L'écart-type est sensible aux valeurs aberrantes (comme la moyenne)
- Si deux séries ont la même moyenne, plus la dispersion est grande plus l'écart-type est grand
- L'écart-type = 0 si toutes les modalités sont les même (car modalité = moyenne)



La formule mathématique de l'écart-type : 

![formule de l'écart-type](images/formule_ecarttype.png)



 Attention ! les statisticiens ont deux façons de calculer la variance et la moyenne selon que l’on estime travailler :
    • sur une population exhaustive : dans ce cas le calcul correspond à la définition que l’on vient de voir et la formule dans Calc est 

```
=VAR.P(plage) et =ECARTYPEP(plage)
```

​    • sur un échantillon : dans ce cas on divise par l’effectif - 1 et les formules dans Calc sont 

```
=VAR(plage) et =ECARTYPE(plage)
```



**La Variance c'est « La moyenne de la somme des carrés des écarts par rapport à la moyenne arithmétique. »**


{% hint style='zob' %}

Ca vous fait peur ! En fait c'est simple il faut reprendre la définition à l’envers :

1. Calculer la moyenne  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_9dfb978d32cbcf5a.png)

2. Calculer l'écart à la moyenne pour chaque modalité  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_5763dbc69e4243d2.png)  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_8c65a86ae6f1b184.png) etc

3. Comme on a des écarts à la moyenne parfois en positif, parfois en négatif, il existe une méthode mathématique pour tout repasser en positif: le carré !

On “élève” donc au carré chaque écart à la moyenne:  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_a23be290f1a035f6.png)    ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_ac474944db97ef37.png) etc  

4. On fait la somme de tout ça:  ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_d1c9677c73d19194.png)

5. On divise par l'effectif:

 ![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_be58c1b43882d35d.png)

→ C'est ça la **VARIANCE** !!

6. On calcule la racine carrée :

![img](file:///C:/Users/soudry/AppData/Local/Temp/lu131925artn8.tmp/lu131925arv5q_tmp_c94105ed42123baa.png)

→ c'est ça l'**ECART-TYPE** !!! {% endhint %}

{% hint style='tip' %}
**Note** : Comme le calcul de la variance se fait à partir du carré des écarts, les unités de mesure ne sont pas les même que celles des observations originales. Par exemple pour des tailles en cm la variance sera donc mesurée en cm².
→ c'est pourquoi on utilise l'écart-type qui, comme il est la racine carrée de la variance, s'exprime dans l'unité de mesure de l'échantillon.{% endhint %}

##### Les quantiles

Les Quantiles sont des caractéristiques de position partageant la série statistique en k parties égales.

Pour k=4, les quantiles appelés quartiles, sont trois nombres Q1, Q2 et Q3 tels que :

   - 25 % des valeurs prises par la série sont inférieures à Q1
   - 25 % des valeurs prises par la série sont supérieures à Q3
   - Q2 est la médiane Me
   - Q3-Q1 est l'intervalle interquartile, il contient 50% des valeurs de la série.

L’intervalle/la distance interquartile c’est l’équivalent de l’étendue pour les 50 % centraux de la série statistique. 

Dans Calc :
pour Q1 

```
=QUARTILE(plage ;1 =) 
```

pour Q3 

```
=QUARTILE(plage ;3)
```


et l’intervalle interquartile 

```
=QUARTILE(plage;3)-QUARTILE(plage ;1)
```

Pour k=10, les quantiles sont appelés déciles, il y a 9 déciles D1, D2,...
10 % des valeurs de l'échantillon sont inférieures à D1,etc...

Pour k=100, les quantiles sont appelés centiles, il y a 99 centiles ; chacun correspond à 1 % de la population.

### Représentation graphique

Les représentations graphiques d’une variable continue ont toutes en commun de permettre d’explorer la distribution de la variable, en identifiant :

- la forme de la distribution
- les concentrations
- les dispersions
- les ruptures dans la distribution

{% hint style='danger' %}
Quelques règles et recommandations :

1. Vérifier les données et donner un titre
2. Supprimer toute information non utile et minimiser l'information secondaire
3. Supprimer les effets inutiles
4. Ajuster les échelles
5. Choisir les couleurs{% endhint %}

{% hint style='info' %}
"message à caractère informatif" sur les courbes : https://youtu.be/OqDx9wxDXUI{% endhint %}

#### La boîte à moustaches ou Boxplot



![boite à moustaches](images/boxplot_1.png) 

Le diagramme en boîte à moustache ou Boxplot permet de représenter schématiquement les principales caractéristiques d'une distribution en utilisant les quartiles.
La partie centrale de la distribution est représentée par une boîte de largeur arbitraire et de longueur équivalente à la distance interquartile, la médiane est tracée à l'intérieur. La boîte rectangle est complétée par des moustaches correspondant aux valeurs suivantes :

   - valeurs supérieures : Q3 + 1,5(Q3-Q1)
   - valeurs inférieures : Q1 – 1,5(Q3-Q1)

Les valeurs extérieures aux « moustaches » sont représentées par des étoiles (ou des points) et peuvent être considérées comme des valeurs aberrantes.

C’est une représentation **unidimensionnelle** efficace pour visualiser une distribution :

- ​	les minima et maxima et donc l’étendue (avec en plus des valeurs identifiées comme aberrantes ou atypiques)
- ​	la position de la boîte nous donne une idée de l'asymétrie potentielle de la distribution

![boite à moustaches détaillée](images/boxplot_2.png)



Cette représentation est optimale pour comparer plusieurs distributions en figurant plusieurs boîtes à moustaches. Il est recommandé de signifier la moyenne à l’aide d’un point superposé.

{% hint style='info' %} Evidemment Calc n’est pas en capacité de faire un diagramme de ce type… mais peut être qu’un vrai logiciel de statistique peut le faire… qui sait ? {% endhint %}



![formes de distribution](images/courbes_distribution.png)



#### Le scalogramme ou matrice ordonnée

Il s’agit d’une représentation **élémentaire** et **unidimensionnelle** (il n’y a qu’un axe : celui des abscisses) d’une distribution statistique, consistant à représenter chaque élément de la distribution par un point sur un axe gradué.
Lorsque deux éléments ont des modalités identiques ou très proches, on procède à un " empilement " des points.

Le diagramme de distribution correspond alors à un histogramme utilisant de très petites classes d’effectifs égaux.

Il s’agit donc un schéma qui permet de visualiser l'ordre et la répartition des différentes valeurs d'une distribution statistique :

- En abscisse, un axe horizontal orienté définit l'échelle de mesure du caractère. Chaque élément est positionné sur cette échelle par un point
- Chaque valeur prise par le caractère est notée par un point rouge
- L’axe des abscisses est gradué en fonction des valeurs du caractère

![scalogramme](images/scalogramme.png)

Le scalogramme est une représentation efficace ; c'est un diagramme de distribution qui permet une première appréhension de la distribution des valeurs d'un caractère :

- visualisation des valeurs maximum et minimum    
- visualisation des zones de concentration (valeurs rapprochées les unes des autres)
- visualisation des zones de dispersion (valeurs éloignées les unes des autres)
- visualisation des discontinuités de la distribution (zones de concentration séparées par un intervalle où les valeurs sont absentes)

{% hint style='tip' %}Ce diagramme de distribution est très utile pour repérer des seuils naturels qui vont permettre une mise en classe de la distribution statistique (discrétisation). Il est possible de repérer aussi les discontinuités apparentes dans la distribution qui vont permettre d’établir les bornes des classes.{% endhint %}

#### Le diagramme en tige et feuilles

Il existe une autre manière de représenter la distribution à l'instar du scalogramme avec en plus la possibilité d'identifier les modalité / les valeurs que prend chaque individu pour la variable continue étudiée : c'est le diagramme en feuille.

Le diagramme en tiges et feuilles ou tracé en arborescence a été inventé par John Tukey (1915-2000). A mi chemin entre le tableau et le graphique ce diagramme est un moyen de présenter toutes les données d’une étude statistique pour en faciliter la lecture d’ensemble.
Le tracé indique comme tige : les premiers chiffres du nombre (milliers, centaines, dizaines...) et comme feuille : le dernier chiffre (unique) de ce nombre.
Les feuilles sont rangées par ordre croissant à l’intérieur d’une tige. Les tiges sont elles même rangées par ordre croissant.

![diagramme en tige et feuilles](images/diag_tige.png)

**Le diagramme revient donc à regrouper les données par classe de même amplitude tout en y faisant figurer l’intégralité des données.**

Les avantages d’une telle présentation sont multiples :

-  Toutes les valeurs y sont nommées et ordonnées 
- Ce tracé ressemble quand on le tourne à un diagramme en bâtons
- On peut y ajouter l’effectif de chaque tige
- Le lecteur peut y lire facilement le nombre de données, la valeur la plus grande, la plus petite, la plus fréquente ainsi que les éventuelles valeurs aberrantes
- **Bonus** : on peut comparer 2 séries statistiques (modalités (feuilles) d'un côté et de l'autre de la tige !)

{% hint style='info' %} Evidemment Calc n’est pas en capacité de faire un diagramme de ce type… mais peut être qu’un vrai logiciel de statistique peut le faire… qui sait ? {% endhint %}



#### Les courbes de fréquence cumulées (ascendantes et descendantes)

**Les courbes des fréquences cumulées ascendantes et descendantes** sont établies à partir du tableau de distribution statistique.

C'est un graphique bi-dimensionnel représentant en abscisse les modalités du caractère continu étudié et en ordonnée les fréquences cumulées.

La construction élémentaire de la courbe des fréquences cumulées consiste à associer à chaque modalité du tableau de distribution statistique sa fréquence cumulée ascendante (% des éléments ayant des modalités de valeur inférieure ou égale) et sa fréquence cumulée descendante (% des éléments ayant des modalités de valeur supérieure ou égale) 

On représente alors sur le graphique les modalités de la variable étudiée (en abscisse) avec leur fréquence cumulée ascendante ou descendante (en ordonnée).

![courbes de fréquences cumulées](images/courbe_freq_cumul.png)

**Interprétation :**

- **Pente forte = concentration**
- **Pente faible = dispersion**
- **Marche d'escalier = rupture**

Le principe :
On part de la variable continue triée par ordre croissant (la taille de chaque stagiaire du plus petit au plus grand)

1. On fait la somme de toutes les valeurs (combien j’ai de centimètres de stagiaires ?)
2. On calcule la fréquence relative pour chaque stagiaire (combien représentent les 152 cm du 1er stagiaire par rapport au total de 2367 cm de stagiaires ?)
3. On y ajoute la Fréquence Cumulée Ascendante (pour chaque individu le % des éléments ayant des modalités de valeur inférieure.) Attention : Le premier individu ne commence pas à 0 mais le dernier individu représente 100% de centimètres de stagiaires !
4. Enfin, on y ajoute la fréquence Cumulée Descendante (c'est à dire le % des éléments ayant des modalités de valeur supérieure.) Attention : le premier individu ne commence pas à 100 % mais le dernier individu représente 0 % de centimètres de stagiaires !



Le diagramme de distribution et la courbe des fréquences cumulées permettent de repérer les **zones de concentration** de la distribution (beaucoup d’éléments sur un intervalle) et les **zones de dispersion** (peu d’éléments sur un intervalle). 

On peut également repérer des **discontinuités**, c’est-à-dire des zones de dispersion séparant deux zones de concentration des éléments. 

Ce type de représentation graphique peu commune dans les disciplines archéologiques est cependant intéressante car elle permet de comparer plusieurs distributions.

:bulb: Fiche : **Stat1_06c_traiter une variable continue (Représentation Graphique)**



#### L'histogramme de distribution

{% hint style='zob' %}"message à caractère informatif" sur les histogrammes qui sont en fait des diagrammes en barres !!
https://youtu.be/OqDx9wxDXUI{% endhint %}

L’histogramme ne doit pas être confondu avec le diagramme en bâton :

-  le **diagramme en bâton** est une représentation graphique d’une ***variable discrète*** : 
  - les barres sont **disjointes** car on veut représenter une différence entre les différentes modalités
  - c’est la hauteur des barres qui indique l’effectif pour chaque modalité
- l’**histogramme** est une représentation graphique d’une ***variable continue*** : 
  - les barres sont **jointives** car justement la série de donnée est continue !
  - selon la construction de l’histogramme, ce n’est pas la hauteur des barres qu’il faut prendre en compte mais la **surface** de celles-ci !

L’histogramme est une **simplification/généralisation** de la série statistique en regroupant dans des classes des valeurs proches. On appelle cela une *partition en classes* **OU** une *discrétisation* : transformer une variable continue en variable discrète.

La construction d’un histogramme est une démarche qui peut paraître longue et laborieuse mais qui permet de comprendre sa lecture.

Il faut :

1. Définir le nombre de classes
2. Choisir une méthode de découpage des classes
3. Construire un tableau de dénombrement
4. Enfin réaliser l’histogramme

:bulb: fiche **Stat1_07a_Discrétisation d’une variable continue (nombre de classes et tableau de dénombrement)**



##### Définir le nombre de classes

Pour faire vite :

- Le nombre de classes ![k](images/k.png) ne doit pas être trop petit = perte d'information, ni trop grand = le regroupement en classes deviendrait inutile et certaines classes pourraient avoir des effectifs trop faibles (voir être vides)
- En général le nombre de classes est compris entre 5 et 20 ; il dépend du nombre ![N](images/nmaj.png) d'observations et de l'étalement des données 
- Il existe des formules/règles pour définir un **nombre maximum** de classes mais elle sont souvent basées uniquement sur l’effectif et sur une distribution normale

{% hint style='info' %}
**Quelques formules magiques**

- *La racine carrée de l’effectif*

Elle ne se base que sur l’effectif (ne prend donc pas en compte les valeurs et la distribution)

* *La formule de Sturges*

Elle ne se base elle aussi que sur l’effectif et est construite à partir d’une loi normale (ne prend donc pas en compte les valeurs et n’est pas faite pour les distributions dissymétriques). 

Il faut savoir que c’est la méthode utilisée par défaut dans certains logiciels (fonction hist() de R par exemple)

- *La formule de Freedman-Diaconis*

Elle ne prend donc pas en compte les valeurs par le biais de l’étendue et de l’intervalle interquartile

{% endhint %}

Au final ces règles formules ne nous donnent qu’**un nombre maximal de classes** et c’est déjà bien ! C’est à nous de définir le nombre de classes en se basant :

- sur la démarche d’analyse que nous avons effectué (calculs des caractéristiques et approche de la distribution par des représentations graphiques)
- sur les données (de quoi parle-t’on ?) et l’objectif de l'histogramme (simple représentation graphique de la distribution ? représentation cartographique ? 



##### Gérer l'amplitude des classes

Le plus simple (mais pas toujours le plus judicieux parfois...) est de définir des classes d'amplitudes égales ; c'est à dire de diviser l'étendue par le nombre ![k](images/k.png) de classes.

Les valeurs d'une classe sont assimilées à la valeur centrale ou centre de la classe. En effet on estime quand on partitionne les modalités d'une variable continue en classe que toutes les modalités à l'intérieur de la classe ont la même valeur.

*Exemple* *: si on regroupe les tibias de longueurs variant entre 36.00 cm et 36.50 cm dans une même classe , cela revient à estimer qu'ils font tous 36.25 cm*

Le regroupement en classes fait perdre aux individus leur caractère propre ainsi que les détails fins de la distribution mais permet une simplification de lecture de la distribution.

##### Etablir le tableau de dénombrement

Le tableau de dénombrement pour les variables continues est plus long à construire que pour une variable discrète car les individus étant regroupés en classe, de nouvelles catégories propres aux classes viennent s’ajouter.

Il se compose généralement de huit colonnes et de ![k](images/k.png)  lignes, ![k](images/k.png) étant le nombre de classes de la partition du caractère. 

![tableau de dénombrement](images/tabl_denombrement.png)



Les classes correspondent à une partition de l'ensemble de l'intervalle de variation de la variable (autrement dit l’**étendue**).

Ces classes doivent être :

- disjointes : l'intersection de deux classes est nulle, un élément ne peut appartenir qu'à une seule classe
- continues : la partition doit être exhaustive, elle doit intégrer toutes les modalités de la variable
- définies par une borne inférieure **Binf** et une borne supérieure **Bsup**, ![n](images/n.png) est le nombre d'éléments compris dans l'intervalle.
  Attention dans cet exemple, les bornes inf sont inclusives et les bornes sup exclusives. On le note [Binf - Bsup[ ou [Binf - Bsup).



Voyons les colonnes dans le détail : 

1. et 2. Les bornes inférieures et supérieures des classes

3. L’**intervalle de classe **: écrit sous forme de texte (sert essentiellement à faire les étiquettes pour Calc, elle est le résultat d’une concaténation des bornes inf. et Bornes sup.) **[Binf - Bsup[**

4. L’**effectif par classe :** c’est, pour chaque classe, le décompte du nombre d’individus dont les modalités sont comprises dans l’intervalle de classe. Noté ![n](images/n.png) sa somme correspond à l’effectif total  ![N](images/nmaj.png).

5. La **fréquence relative** (appelée aussi **fréquence simple**) **:** rapport entre l’effectif par classe ![n](images/n.png) et l’effectif total ![N](images/nmaj.png).

**f = n/N**

6. L’**amplitude de classe :** *Note :* dans le cas d’une partition en classes d’amplitudes égales elle est égale à l'étendue divisée par le nombre de classes  ![k](images/k.png) . 

**A = Bsup - Binf**

7. Le **centre de la classe: ** C’est la moyenne des bornes de la classe. Cette valeur permet de résumer une classe par un seul chiffre.

**(Bsup+Binf)/2**

8. La **densité de probabilité** (appelée aussi **fréquence moyenne**) : c’est la fréquence relative divisée par l’amplitude de classe.

    **f/A**



*Ces valeurs permettent de construire un histogramme “juste”* ***même si les classes ne sont pas d’amplitude égale*** *et sont reportées sur l’axe des ordonnées de l’histogramme.*

*En effet, dans ce cas* ***les rectangles*** *(les “barres” de l’histogramme) ont alors* ***une surface*** *(amplitude x fréquence relative)* ***proportionnelle à la part des effectifs de chaque classe****.* ***La surface totale des rectangles est égale à 1.***

![tableau de dénombrement 2](images/tabl_denombrement_2.png)



# 4. Les règles de sémiologie graphique

Historique, règles, recommandations 



# Pour aller plus loin

Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) The New S Language. Wadsworth & Brooks/Cole.

Venables, W. N. and Ripley. B. D. (2002) Modern Applied Statistics with S. Springer.