⚠️ Ceci est une version PDF du document Formateur de la 1ère partie de la formation  SI CORRECTIONS a minima les indiquer en ROUGE dans le document partagé: https://docs.google.com/document/d/15xSYw_8E4cYC0LuPO1UUPbY1TKsb0HdJi1biixivuqM/edit ⚠️ 

**Formation 0 - Les données** ![img](https://lh7-us.googleusercontent.com/docsz/AD_4nXfweaiMh8WxS6_vBpk_6ekdpjBT4oAkdzYe4AjpTO-j_F_NGIbs2J4qX4AbGh-LCDYIYlrCo2OWGYyGNFTN-5DplALLwjBPmBTsdfAxsvY0SwjVq6O3S2s0dQkRuBoLci54zN3fKbl5B_hWl3Bu7liXlEI?key=Qymrg2oZWUrUMyOGbb0n-A)

**archéologiques : préparer et consolider ses tableaux** 

# **Supports de présentation**

> [!CAUTION]
>
> tentative d'un nouveau support pour le diaporama directement dans gitlab → https://formationsig.gitlab.io/diapo_stat

La référence au diaporama créé sur slide.com (https://slides.com/archeomatic/socle/) est dictée par la forme du diaporama organisé en colonnes au sein desquelles les diapositives sont numérotées de 1 à n du haut vers le bas : 

ex : 2.3 = la 3e diapositive de la 2e colonne.

Constitution du diaporama : 

- la première colonne (4 diapos) sert uniquement à introduire la formation
- les 3 colonnes suivantes se rapportent à la première partie de la formation (matinée)
- La dernière colonne se rapporte à la seconde et dernière partie de la formation.

Le numéro des slides et des colonnes apparaît en bas à droite de l’écran, à côté du schéma de navigation : 

![img](https://lh7-us.googleusercontent.com/docsz/AD_4nXcZOUDXZxJe44NtORTWQYeAISsarcrg41hSXt9sAFX0vrlXGCIVOWc2T7Ej8F_SGazmmXipWsYV3waPc2J6V7iJHvIJpprsqh1CNa8CUt3uiQXz-B3XNTLUy19uHCUTLOzNVW9PU9fAoUX1uysCMrPM92XD?key=Qymrg2oZWUrUMyOGbb0n-A)

Le chapitrage du document reprend la numérotation des diapositives dans la numérotation des chapitres/parties.

# **Introduction**

*(colonne 1 du slides)*

**Dia 1.1/1.2** 

Cette formation est la clé de voûte de toutes les formations dédiées à la collecte et l’exploitation des données : formations SIG, formations Stats, enregistrement stratigraphique.. car le tableau est l’élément central ou transversal du cycle de vie des données. Les données numériques telles qu’on les manipule sont toujours structurées en lignes et en colonnes prenant tantôt la forme d’un simple tableur, d’une base de données ou d’un tableau en csv archivable.

Note : le csv est un fichier dans lequel les données sont séparées/structurées par des points-virgules et non des colonnes. C’est le format informatique (.xls, .calc, .csv…) .et les outils de gestion utilisés (SIG, SGBDR, tableur) qui changent ; pas la structuration des données elle-même qui relève uniquement d’une démarche intellectuelle qui reflète la problématique abordée.

La formation est divisée en deux volets:

- un premier volet théorique centré sur la lecture et la compréhension des tableaux de données archéologiques, de manière générale ; on essaye de décrypter des tableaux pour en comprendre la structuration ; théoriquement, la structure du tableau doit renseigner l’utilisateur sur la problématique du producteur du tableau ; on aborde de manière synthétique le cheminement pour passer d’un tableau à une base de données ; 

- un second volet, pratique, qui vise à utiliser Calc pour nettoyer, créer et/consolider des tableaux de données afin de pouvoir les manipuler dans le cadre d'une étude archéologique avec un tableur, un logiciel de statistique ou de SIG. Cette étape peut être faite avec un tableur en utilisant les fonctions courantes (depuis le tri jusqu'au Tableau Croisé en passant par les fonctions). 

**Dia 1.3 objectifs de la formation** 

- produire des tableaux propres et exploitables à l’aide d’un tableur
- comprendre l’importance du tableau, élément central du cycle de vie des données (pour la collecte, l’exploitation et la restitution - produits de la recherche - des données)
- maîtriser et appliquer les **10 commandements** du tableau de données

**Dia 1.4 Programme de la formation**

matinée : lire et comprendre des tableaux de données

1. théorie du tableau
2. exercice 1 : autopsie des tableaux exemples
3. exercice 2 : décrypter un tableau-base de données et en tirer la structure

 

après-midi : utilisation du tableur pour préparer et consolider ses tableaux 

1. nettoyer le tableau de données : rechercher/remplacer, filtrer/trier, doublons, adressage
2. exploiter : tableau croisé dynamique



# **lire et comprendre des tableaux de données** 

*(colonne 2 du slides)*

**Dia 2.1 et 2.2**

> :information_desk_person: **Consignes formateur :**
>
> Lire les diapos de la colonne 2, tout est dessus:
>
> * diapo 2.7 : l’image représente une structure de base de données pour illustrer le fait que la table d’une base de données relationnelle est un tableau (il faut imaginer les champs en colonne et non en ligne)
> * les diapos 2 à 12 illustrent la diversité des tableaux utilisés au quotidien
> * les diapos 13 à 20 illustrent la “typologie” des tableaux qu’on en déduit : tableau-formulaire, tableau-figure, tableau-base de données, tableau-calcul

# **Exercice 1 : y’a-t-il un bon et un mauvais tableau?**

*(colonne 3 du slides)*

**Dia 3.1 et 3.2**

>  :information_desk_person: **Consignes formateurs : ** 
>
> * faire ouvrir le tableau sur le drive : à chaque feuille correspond un exemple de tableau
> * lire les consignes stagiaires
> * décrypter le premier tableau collectivement
> * puis diviser l’assistance en deux : une partie des stagiaires examine les tableaux de 2 à 6, les autres stagiaires s’occupent des tableaux 7 à 12
> * faire intervenir les stagiaires pour passer en revue chaque tableau et les qualifier
> * écrire sur le *paperboard* les caractéristiques que doivent respecter les tableaux et qui vont dessiner progressivement les 10 commandements 



rappel des consignes stagiaires : :warning: **À écrire sur le Paper Board**

pour chaque tableau, préciser :

\- le sujet du tableau (unité d'enregistrement)

\- si les données sont manipulables en l’état (peut-on directement, trier ou calculer...)

\- en déduire l'objectif/le type de tableau (calculs, bdd, formulaire, présentation)



**Correctif**

en réalité, tout va de paire : pour identifier l’unité d’enregistrement (le sujet du tableau) on a parfois besoin de manipuler les données. Donc on répond souvent en même temps au 2 premières questions qui nous amènent naturellement à en déduire le type de tableau ;



> :information_desk_person: **formateurs rassurez-vous** : ne cherchez pas à répéter exactement ce qu’il y a dessous ; l’objectif de l’exercice est de faire apparaître progressivement les **10 commandements** ; **ils apparaissent presque tous naturellement, même en laissant les stagiaires faire, faites-vous confiance ; **l’idée, c’est de les écrire au tableau au fur et à mesure où ils apparaissent tableau après tableau**.



**1_ FXXXXXX_inv_str**

:grey_question: **sujet (individu, enregistrement) :** us

Les données ne sont pas exploitables sans manipulations préalables parce que :

\- mise en forme pour les relations stratigraphiques (on ne peut pas rechercher par type de relation sans supprimer auparavant la cellule fusionnée « relation stratigraphiques ») ; 

\- plusieurs infos dans un même champs :

\- champ N° de fait ( et d’ailleurs : une US peut-elle être commune à deux faits différents ??? )/N° de sondage : F= Fait, M= Mur + le numéro d’enregistrement ; la lettre en préfixe n’est pas nécessaire si elle sert à la façon d’une étiquette (comme avec les sond.1 = c’est redondant, ça brouille la lecture, ça contraint le type de champ à de l’alphanumérique donc pas forcément adapté aux tris) , si la lettre apporte une information supplémentaire ; dans ce cas, créer un champ supplémentaire qui permette, dans le cas présent, de trier par type de structure ; 

\- relations stratigraphiques “equivalent”, ligne 19 : l’us est suivie d’un point d’interrogation ; ce sont deux informations dont une information d’incertitude ; 

:checkered_flag: **objectif/type de tableau :** tableau-présentation (cas typique de tableau pour un inventaire de rapport)

> **10 Commandements : **
>
> :white_check_mark: **pas de mise en forme (les cellules fusionnées font partie de la mise en forme)**
>
> :white_check_mark: **une information par champ ; l’incertitude est une information**



**2_PCR_datations**

:grey_question: **sujet (individu, enregistrement) :** l’échantillon

Le tableau ne peut pas être manipulé pour exploiter les données qu’il contient sans un nettoyage préalable

- mise en forme (titre, légende, lignes vides destinées à séparer les sites
- la couleur est porteuse d’information : il faudrait la traduire en donnée (une colonne «priorité date » avec 1 ou 0) ; cette information risque d’être perdue si elle n’est pas transposée en données dans le tableau ; 
- identifiant unique en colonne “SAMPLE REFERENCE” mais hétérogénéité de l’identifiant 

Le tableau qui fonctionne quand même si on enlève la mise en forme : on peut faire un décompte du nombre d’échantillons par matériaux par exemple ou par site.

:checkered_flag: **objectif :** présentation , c’est un tableau-figure (titre, légende, auteurs…) 

> **10 Commandements : **
>
> :white_check_mark: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :white_check_mark: une information par champ ; l’incertitude est une information
> 
> :white_check_mark: **cellules vides = pas d’informations, différent de 0**



**3_Inv_SLC**

:grey_question: **sujet (individu, enregistrement):** 

\- on doit faire un tri préalable pour comprendre ; toutes les premières colonnes sont destinées à localiser l’individu (colonnes de contexte) : Tr, Sd, identif US, Iso (l’ISO peut renvoyer à l’échantillon)

\- l’individu serait le NR/NMI par type de matériaux par structure (on a plusieurs individus venant de du même fait ; c’est la colonne « roche » qui permet de les différencier ; on peut avoir plusieurs fgts d’une même roche (un seul individu)

\- l’absence de numéro d’identifiant unique nuit à la lecture du tableau

| 50   | 4    |      | Fosse | HMA  | Carolingien | Calcaire | calcaire fin        | XX   | 7    | 7    | 1011 | 1,011 |      |      |      |      | bloc, débris |      |      |      |      |      |      |      |      |
| ---- | ---- | ---- | ----- | ---- | ----------- | -------- | ------------------- | ---- | ---- | ---- | ---- | ----- | ---- | ---- | ---- | ---- | ------------ | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 50   | 4    |      | Fosse | HMA  | Carolingien | Meulière | meulière caverneuse | X    | 6    | 6    | 400  | 0,4   |      |      |      |      | bloc, débris |      |      |      |      |      |      |      |      |

Cellule vide : pas d’information

:checkered_flag: **objectif :** enregistrement/inventaire = tableau base de données

structure interne un peu complexe mais ça marche : données manipulables, on peut faire un tableau croisée dynamique.

> **10 Commandements : **
>
> :white_check_mark: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :white_check_mark: une information par champ ; l’incertitude est une information
>
> :white_check_mark: cellules vides = pas d’informations, différent de 0
>
> :white_check_mark: **identifiant unique bien visible/individus en ligne et variables en colonne**



**4_DXXXXXX_inv_mob**

:grey_question: **unité/sujet :** type de mobilier par fait et par US mais celle-ci n’est pas remplie (même cas que **Inv_SLC, FXXXXXX_inv_céramique)**

données exploitables : oui mais bof : 

- colonne avec de l’alphanumérique inutile (“sond.1”)
- problème entre l'intitulé de la colonne “type de matériaux” et certaines valeurs : clou, épingle, crochet : ?
- colonne 

:checkered_flag: **objectif :** inventaire/base de données (mais on pourrait avoir envie de faire des calculs)

> **10 Commandements : **
>
> :white_check_mark: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :white_check_mark: une information par champ ; l’incertitude est une information
>
> :white_check_mark: cellules vides = pas d’informations, différent de 0
>
> :white_check_mark: identifiant unique bien visible/individus en ligne et variables en colonne
>
> :white_check_mark: **donc pas de doublon**



**5_Rapport_CTRA**

:grey_question: **sujet (individu, enregistrement) :** le centre archéologique? mais en réalité, ce que veut montrer ce tableau c’est le nombre de rapports par type et par région

 Données non exploitables dans manipulations préalables

- format image
- sous-totaux intégrés au tableau (assimilable à de la mise en forme)

:checkered_flag: **objectif:** tableau-communication qui devait être à la base un tableau-calculs

> **10 Commandements : **
>
> :white_check_mark: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :white_check_mark: une information par champ ; l’incertitude est une information
>
> :white_check_mark: cellules vides = pas d’informations, différent de 0
>
> :white_check_mark: identifiant unique bien visible/individus en ligne et variables en colonne
>
> :white_check_mark: pas de doublon
> 
> :white_check_mark: **travailler dans un format informatique permettant l’exploitation des données du tableau et leur export (tableur ou .csv)**



**6_Inv_coquillages**

:grey_question: **sujet/individu (individu, enregistrement) :** 

- unité pas claire à la première lecture (ni à la deuxième d’ailleurs) : on pense d’abord à la caisse mais non
- il faut trier sur caisse, puis sur Fait puis sur US ; 
- l’unité serait le nombre de sacs par US, par Fait, par caisse (bof bof)? On peut avoir x sacs de la même US, du même Fait dans plusieurs caisses différentes ; 
- pas d’identifiant unique
- champ « mobilier » qui peut renvoyer aussi bien à un type de matériau/mobilier (crustacé) qu’à un type de structure
- ce tableau est difficilement utilisable en dehors de l’objectif pour lequel il a été probablement conçu : identifier le contenu de chaque caisse ou localiser le mobilier (sac) de chaque us de chaque fait? le nom même de la feuille est trompeur : “liste de caisse” : on s’attend à ce que l’unité soit la caisse ; ce n’est pas le cas ; 
- en l’état, ce tableau ne semble répondre ni à l’objectif annoncé (liste de caisses), ni à ce qu’aurait pu être le tableau élémentaire (on ne peut même pas compter le nombre de sac total) ;

:checkered_flag: **objectif :** inventaire tableau-base de données ? Objectif pas très clair

> **10 Commandements : **
>
> :white_check_mark: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :white_check_mark: une information par champ ; l’incertitude est une information
>
> :white_check_mark: cellules vides = pas d’informations, différent de 0
>
> :white_check_mark: identifiant unique bien visible/individus en ligne et variables en colonne
>
> :white_check_mark: pas de doublon
>
> :white_check_mark: **travailler dans un format informatique permettant l’exploitation des données du tableau et leur export (tableur ou .csv)**



**7_FXXXXXX_inv_céramique**

:grey_question: **sujet/individu (individu, enregistrement) :**  lot de céramique

données exploitables

:checkered_flag: **objectif :** tableau base de données

Même cas de figure que inv_SLC avec l’identifiant unique en plus qui facilite la lecture !

> **10 Commandements : **
>
> :white_check_mark: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :white_check_mark: une information par champ ; l’incertitude est une information
>
> :white_check_mark: cellules vides = pas d’informations, différent de 0
>
> :white_check_mark: identifiant unique bien visible/individus en ligne et variables en colonne
>
> :white_check_mark: pas de doublon
>
> :white_check_mark: travailler dans un format informatique permettant l’exploitation des données du tableau et leur export (tableur ou .csv)
>
> :white_check_mark: **veiller à l’homogénéité de la saisie des valeurs pour en simplifier la lecture et l’exploitation**

ex : colonne “Groupe tech.” on trouve : 

“amph_ita” ou “Amphore italique”



**8_DXXXXXX_inv_faits** 

:grey_question: **sujet/individu (individu, enregistrement) :**  le fait

données exploitables

:checkered_flag: **objectif :** tableau-inventaire/base de données

Rien à dire, ça marche.



**9_marché_reprographie** 

:grey_question: **sujet/individu (individu, enregistrement) :** on sait pas trop

données non exploitables

:checkered_flag: **objectif :** tableau-formulaire

Exemple-type de l’utilisation détournée d’un tableur utilisé ici pour structurer un formulaire

Les Données qu’il contient ne peuvent pas être manipulées.



**10_DST_jours_ope_2019** 

:grey_question: **sujet/individu (individu, enregistrement) :** unité de laboratoire

données non exploitables telles qu’elles : 

\- mise en forme : titre, cellules fusionnées

\- X infos par champs : on ne pourra pas facilement exploiter le champ « type d’opération préventive »

\- si l’objectif du tableau était de qualifier et de quantifier le nombre de jours opération par unité comme l’indique le titre, on aurait pu conserver les unités en lignes et créer autant de colonnes que de sigles (F, PF, RFO) : ça aurait simplifié les comptes ; on aurait pu aussi mettre les sigles en lignes et les unités en colonnes (variables). Tout dépend de ce sur quoi on travaille ;

-inadéquation titre et structure du tableau ;

\- intitulé de colonnes qui comportent des consignes de saisie ou une légende…

:checkered_flag: **objectif :** présentation/figure (titre et mise en forme) et calcul (somme indiquée en bas de la dernière colonne)

> **10 Commandements : **
>
> :white_check_mark: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :white_check_mark: une information par champ ; l’incertitude est une information
>
> :white_check_mark: cellules vides = pas d’informations, différent de 0
>
> :white_check_mark: identifiant unique bien visible/individus en ligne et variables en colonne
>
> :white_check_mark: pas de doublon
>
> :white_check_mark: travailler dans un format informatique permettant l’exploitation des données du tableau et leur export (tableur ou .csv)
>
> :white_check_mark: veiller à l’homogénéité de la saisie des valeurs pour en simplifier la lecture et l’exploitation
>
> :white_check_mark: **intitulés de colonne clairs, explicites, concis**
>
> :white_check_mark: **éviter les caractères spéciaux dans les valeurs et dans les intitulés de colonne** 

Dans le domaine des BDD, les caractères spéciaux peuvent invalider la création d’un champ - colonne + Les expressions de requête informatiques incluent des champs qui contiennent des caractères spéciaux= conflits potentiels) ; mêmes les accents sont à éviter (on peut contourner en donnant des alias?).



**11_extraction_SGA (facultatif)**

:grey_question: **sujet/unité :** on ne sait pas trop…. Le code sga n’est pas unique et on peut trouver X enregistrements avec le même code SGA, la même année, le même responsable… mais pas exactement le même titre ; l’extraction peut peut être incomplète ; à mon avis, l’identifiant unique, c’est un intervalle de temps (une planification dans le calendrier d’une tâche se rapportant à) 

données exploitables : oui, on peut trier, on doit pouvoir faire un tableau croisé dynamique etc

:checkered_flag: **objectif :** tableau-base-de-données

> **10 Commandements : **
>
> :one: pas de mise en forme (les cellules fusionnées font partie de la mise en forme)
>
> :two: une information par champ ; l’incertitude est une information
>
> :three: cellules vides = pas d’informations, différent de 0
>
> :four: identifiant unique bien visible/individus en ligne et variables en colonne
>
> :five: pas de doublon
>
> :six: travailler dans un format informatique permettant l’exploitation des données du tableau et leur export (tableur ou .csv)
>
> :seven: veiller à l’homogénéité de la saisie des valeurs pour en simplifier la lecture et l’exploitation
>
> :eight: intitulés de colonne clairs, explicites, concis
>
> :nine: éviter les caractères spéciaux dans les valeurs et dans les intitulés de colonne
>
> :one::zero: Inspection visuelle des données (coquilles, orthographe, *etc*)



**12_PCR1**

**diapos 3.3 à 3.7**

A lire.

# **Exercice 2 : décrypter un tableau-base de données et en tirer la structure simplifiée du MCD**

*(colonne 4 du slides)*

**dia 4.1 à 4.3 :**  consignes exercices

**feuille PCR_MAISONS1_UNITE du fichier du drive** 

>  :information_desk_person: Consignes formateur :
>
> 1. afficher le tableau
> 2. passer en revue chacun des champs pour trouver l’unité d’enregistrement
> 3. regrouper les champs qui vont ensemble comme dans le corrigé

“correction” regroupement des champs : se trouve dans le même fichier, sur la feuille : **correction_PCR_MAISONS1_UNITE**

**légende :** 

:green_heart: **vert : données se rapportant à l’opération (dont des données de localisation)**

:blue_heart: **bleu : données relatives à ce qu’on veut étudier : les unités d’habitation (sur le correction tous les champs sont regroupés mais montrer dans la démonstration qu’on pourrait les dissocier ; voir MCD 2)**

:yellow_heart: **jaune : données de localisation géographique se rapportant à la localisation des unités d’habitation (à regrouper avec les données de l’uh dans un premier temps avant de les dissocier pour montrer le rôle du SIG)**

:grey_question: **gris : les métadonnées et les données de suivi**

:heartpulse: rose : les sources (références bibliographiques)

> 4. dessiner au tableau la structure de base de données relationnelle simplifiée qu’on peut en déduire (MCD1):
>    	* Commencer par dessiner la table des UH, puis celle des opérations et de la biblio
>     * notions élémentaires de base de données à rappeler mais sans rentrer des les détails : 
>       * tables
>       * liens de cardinalités qui formalisent les relations entre les entités de deux tables (1 à 1, 1 à n, n à n )

![img](https://lh7-us.googleusercontent.com/docsz/AD_4nXcPIYxPKL4zGiHkxEzFnPzxEb-Ty3-0bj8JmvVNgesquDzeFSWNgcDTI1m7TsRIDTRWGMyTu5dBFx5iiUQgVnvXEDuUPEOsWILCJI5SD_KrAx-3COOLWhqhnqDGTfw3uvx80Mc8ym2KD6h4tU4V9vMGT-E?key=Qymrg2oZWUrUMyOGbb0n-A)

>  Compléter le premier MCD avoir avoir re- consulté le tableau : certaines colonnes mentionnent plusieurs valeurs : colonne “infrastructures_extérieures” et “bâtiments” par exemple. Expliquer qu’on pourrait améliorer le MCD en créant une table “infrastructure_ext” et une table “bâtiment” si on veut y consigner davantage d’infos :

![img](https://lh7-us.googleusercontent.com/docsz/AD_4nXc3FU-UE4ShT2Hm_2Y6c1rwKmKRHOUElBvAnn0LuNqDT0upQeGnr8NzloyVz_Rem4gU4u5AaiR03aRXVmSfoYRQXT2mMeurRXrurZlYf1QL2k6rt6CjjoIDfdkVY-dTxGaeLBmgOWcAddl7xjgEtFXgWIQ?key=Qymrg2oZWUrUMyOGbb0n-A)

> Pour les métadonnées, ne pas s’attarder : ici elles sont représentées comme des données au sein de la base, rattachées à l’UH. Ce n’est pas forcément pertinent, d’autant plus qu’on ne peut pas réellement les attacher à l’une ou l’autre des tables.

>  BONUS: Pour perspective et ouverte d’esprit : 
>
> Montrer où se niche le SIG : dans les éléments de localisation de la table “opération” et dans les coordonnées des unités d’habitation”.
>
> Montrer que les tables “Opération” et “Biblio” relèvent de la gestion des sources qui tend à être centralisée au niveau des institutions et ou des ministères (voir au-delà) et que peut-être, un jour, les archéologues de l’Inrap n’auront plus à gérer leurs sources par des tables dédiées en vase clos à l’échelle d’un projet mais qu’il suffira d’associer (au sein d’une table dédiée ou pas) un lien vers Caviar, Patriarche, Dolia, le SI opération (SGA), Nakala…) pour faire le lien avec la documentation/données sources.

![img](https://lh7-us.googleusercontent.com/docsz/AD_4nXdTizObW0QY8sX_wi6sbrKvpqBY9xAXJ9Oq0fe0gTLAbD-jV5C_M5Fqdrxge5CyStFkRZDokQjOp7FLP8j9SPgPUhPEZ9VSo3RDgxpUss3XjZYM5qCCYUqQJpjKfF6YPtmSDMSZQHxVSyAgxaWARNSAKWXG?key=Qymrg2oZWUrUMyOGbb0n-A)

**dia 4.4 :** passer en revue les **10 commandements** pour terminer cette première partie.