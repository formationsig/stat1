# Summary



## module socle 1.0: Les données archéologiques : préparer et consolider ses tableaux




* [module socle 0 - Les données archéologiques : préparer et consolider ses tableaux ](module0.md)




## Ressources

* module socle 1.1  - Les données archéologiques : décrire, analyser et représenter (1 variable) ](/mod_1_2_stat/mod1_2_stat.md)
* module socle 1.2 - Les données archéologiques : initiation aux traitements de données sous R (1 variable) ](/mod1_2_initR/mod1_2_initR.md)

