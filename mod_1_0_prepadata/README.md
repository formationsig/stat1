# ![mod1_clean](images/mod1_clean.png) **module socle 1.0** - Les données archéologiques : préparer et consolider ses tableaux

Ce module socle est destiné à tous les agents ayant des données sous la forme de tableau a créer et/ou traiter. Il est un socle commun préalable et indispensable pour suivre toutes les formations Stat et SIG 



**objectifs: ** 

* Préparer et consolider des données archéologiques dans un tableau élémentaire en vue d'un futur traitement statistique .
* Utiliser un tableur pour nettoyer, consulter et manipuler des données (depuis le tri simple jusqu'au tableau croisé dynamique)
* Acquérir le vocabulaire simple de description et de caractérisation d'un tableau de données. 

**logiciels utilisés:** tableur (Libre Office)

**durée**: 1 a 2 jours

**prérequis:** aucun ! ce module est destiné à tous les agents. Il est en revanche un prérequis pour toutes les formations SIG et Stat.

 

> <u>Note:</u> ce module est une réécriture d'une partie de l'ancienne formation Stat 1. 



##  Ressources:

* le [diaporama en ligne (***reveal.js + gitlab***)](https://formationsig.gitlab.io/diapo_stat) de ce module: (*work in progress*)



[ :leftwards_arrow_with_hook: Accueil - Formation SIG](https://formationsig.gitlab.io/toc/)



Toute cette documentation est sous licence sous licence [![CC](images/CC_BY_ND.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)