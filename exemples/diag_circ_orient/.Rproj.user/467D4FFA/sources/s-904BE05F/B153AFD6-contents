# import des données
tab <- read.csv2("F109400_axes_squelettes.csv")
or <- tab$orient

# analyse statistique d'une donnée continue
# avec des mesures caractéristiques
summary(or)
sd(or)
IQR(or)
range(or)
#=> 118 mesures allant de 239° à 310° se qui fait 71° d'écart au max
# un écart type  de 12 degrés
# Attention j'imagine que les orientations ont toujours été prises "dans le même sens "
# et que ceci explique qu'il n'yen ai aucune entre 0 et 180°
# ou autrement dit qu'une sépulture orientée à 270° correspond aussi à 90°

# avec des représentations graphiques: Moustache Fever !
boxplot(or, horizontal=T) # je fais une boxplot
par(new=T) # le prochain graph sera superposé
stripchart(or, pch=20, method="stack") # j'ajoute un scalogramme
points(x=mean(or), y=1, pch=19, col="red") # et un point rouge pour la moyenne
#=> on voit bien avec cette double représentation:
# - qu'il y a quelques valeurs atypiques entre 239° et 258°
# - que 50% des valeurs se situent entre 279°(Q1) et 293° (Q2) avec un écart inter quartile de 14°
# (la moitié des mes mesures d'orientations au centre de la distribution présentent une différence de 14°
#   entre la plus petite mesure et la plus grande))

# Maintenant pour partitionner cette variable en classes c'est encore autre chose
# l'idéal est de bien connaitre ses mesures et de savoir si elles correspondent à quelque chose sur le terrain
# aussi à partir de combien de degrés peut on être assuré d'une différence (selon la pertinence des mesures de terrains par ex)
# il est donc a ce stade plus facile de jouer avec discretisator: https://archeomatic.shinyapps.io/Discretisator/

# mais a vu de nez 5 classes avec Jenks ou 7 en écart-type me semble pas mal...
# l'une et l'autre on la moyenne (env 285°) comme coupure centrale...

# pour faire un diagramme circulaire
# version originelle
library("plotrix")
# tableau de dénombrement à partir de la variable orientation 
recap <- aggregate(or,by=list(or),length)
# diagramme circulaire
diag <- polar.plot(c(0,recap$x),c(0,recap$Group.1),
                   main="Histogramme circulaire \nd'orientations",
                   #titre
                   start=90,
                   # origine du 0
                   clockwise=TRUE,
                   # sens des aiguilles d'une montre
                   lwd=2,
                   # épaisseur des traits
                   line.col="blue",
                   # couleur des lignes ??
                   show.grid.label=3,
                   # affichage de la grille des étiquettes
                   show.radial.grid=TRUE,
                   # affichage de la grille intérieure
                   labels=seq(0,350,by=10),
                   # définition des étiquettes (séquence de 0 à 350, tous les 10)
                   label.pos=seq(0,350,by=10))
# emplacement des étiquettes (séquence de 0 à 350, tous les 10)

# => d'accord c'est pas très joli mais c'est parce que les mesures sont assez ramassées de 239 à 310

# On pourrai essayer de voir si on tracait les symétriques (une sep orientée à 270° correspond à 90°)
# j'ajoute à la liste des orientations une liste correspondant aux orientations - 180°
or2 <- combine(or, or-180)
# je retrace le graph circulaire
recap <- aggregate(or2,by=list(or2),length)
# diagramme circulaire
diag <- polar.plot(c(0,recap$x),c(0,recap$Group.1),
                   main="Histogramme circulaire \nd'orientations",
                   #titre
                   start=90,
                   # origine du 0
                   clockwise=TRUE,
                   # sens des aiguilles d'une montre
                   lwd=2,
                   # épaisseur des traits
                   line.col="blue",
                   # couleur des lignes ??
                   show.grid.label=3,
                   # affichage de la grille des étiquettes
                   show.radial.grid=TRUE,
                   # affichage de la grille intérieure
                   labels=seq(0,350,by=10),
                   # définition des étiquettes (séquence de 0 à 350, tous les 10)
                   label.pos=seq(0,350,by=10)) # emplacement des étiquettes (séquence de 0 à 350, tous les 10)

# => perso je trouve cela plus lisible même s'il faut faire attention à la lecture car les effectifs sont doublés

# Essai un peu plus juste:
or3 <- combine(or, or-180)
# je retrace le graph circulaire
recap <- aggregate(or3,by=list(or3),length)
# diagramme circulaire
diag <- polar.plot(c(0,(recap$x/2)),c(0,recap$Group.1),
                   main="Histogramme circulaire \nd'orientations",
                   #titre
                   start=90,
                   # origine du 0
                   clockwise=TRUE,
                   # sens des aiguilles d'une montre
                   lwd=2,
                   # épaisseur des traits
                   line.col="blue",
                   # couleur des lignes ??
                   show.grid.label=3,
                   # affichage de la grille des étiquettes
                   show.radial.grid=TRUE,
                   # affichage de la grille intérieure
                   labels=seq(0,350,by=10),
                   # définition des étiquettes (séquence de 0 à 350, tous les 10)
                   label.pos=seq(0,350,by=10)) # emplacement des étiquettes (séquence de 0 à 350, tous les 10)

