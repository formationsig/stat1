---
title: "Sériation avec les packages seriation et ggplot"
author: "Sylvain Badey - Inrap"
date: "05/05/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Les scripts d'origine

### Script 1: sériation avec ggplot

```{r, eval = FALSE}
library(ggplot2)
library(GGally)
library(tidyverse)
library(tidyr)
library(bertin)
library(seriation)

data <- read.csv2("classeur2.csv", header = TRUE, stringsAsFactors = FALSE, fileEncoding = "UTF-8-BOM")
# On a déjà affaire a un tableau de synthèse (de contingence) avec:

data %>%
gather(groupestec, value, ind:groupe.4p, factor_key=TRUE) %>%
ggplot() +
aes(x=groupestec, y=periodes, size=value) +
scale_size(range=c(3,40)) +
aes(color=periodes) +
theme(axis.text.x = element_text(angle = 45, hjust=1)) +
geom_point(alpha = 0.5)
```

### Script 2: Permutation matricielle de Bertin

```{r, eval = FALSE}
library(bertin)
library(seriation)

data <- read.csv2("d:/classeur2.csv", header = TRUE, stringsAsFactors = FALSE, fileEncoding = "UTF-8-BOM")

data <- head(apply(data[,-1], 2, rank), 28)
row_order <- seriate(dist(data, "minkowski", p = 1),  method ="TSP")
col_order <- seriate(dist(t(data), "minkowski", p = 1),  method ="TSP")
orders <- c(row_order, col_order)
plot.bertin(data, palette=c("white","grey","black"))
#ou :
bertinplot(data, orders, options = list(panel=panel.squares, spacing = 0, frame = TRUE))
```

### La question:
 
 >Après plusieurs recherches infructueuses sur le net, je me tourne vers
toi pour obtenir si possible un petit coup de main dans l'utilisation de R.
J'ai un tableau simple, à deux entrées (périodisation, classes techno
céramique, valeurs).
J'obtiens (script1, test) un graphique de pondération sur ces deux
paramètres, en utilisation la fonction gather puis ggplot. Mais mon
objectif final serait de redistribuer - sériation - les lignes et
colonnes en diagonale.
J'ai tenté avec le package Bertin, mais il offre (a priori) moins de
souplesse pour identifier les paramètres, gérer la pondération, les
couleurs etc. (cf test2).
Je souhaitais donc savoir s'il était possible de redistribuer sur la
fonction gather et comment. Dans le cas contraire, verrais-tu une autre
solution ?

## Réponse:

Tentative de solution en reprenant la méthode de sériation du package eponyme et la représentation graphique de ggplot.


### Importation des données

```{r}
setwd("~/SLY_Perso/CHANTIERS/2020_FMarembert")
```
→ répertoire de travail a modifier selon le poste

```{r}
tab <- read.csv2("classeur2.csv"
                 , header = TRUE, stringsAsFactors = FALSE, fileEncoding = "UTF-8-BOM"
                 , row.names = NULL)
```
> Note: pas de nom de colonnes, car format nécessaire pour la transformation en tableau long.

### Activation des packages nécessaires

```{r, message=FALSE}
library(ggplot2)
library(dplyr)
library(tidyr)
# library(bertin)
library(seriation)
```

### Sériation en lignes (périodes) et en colonnes (groupes techniques)

Démarche:

* réimporter la table de données avec des noms de lignes (avec le paramètre `row.names=1`)
* faire la sériation sur les lignes (fonction `seriation::seriate`) → transformer la liste en vecteur (`as.vector(unlist())`)
* réordonner les lignes (fonction `dplyr::slice`) du tableau d'origine selon cette liste
* faire la sériation sur les colonnes → transformer la liste en vecteur
* réordonner les colonnes (fonction `dplyr::select`) du tableau d'origine selon cette liste 

#### Ré-importation des données avec des noms de lignes
```{r}
tab2 <- read.csv2("classeur2.csv", header = TRUE, stringsAsFactors = FALSE, fileEncoding = "UTF-8-BOM", row.names = 1)
```

#### sériation des lignes (périodes) 


```{r}
row_order <- as.vector(unlist(seriate(dist(tab2, "minkowski", p = 1),  method ="TSP")))
row_order <- tab %>% slice(row_order) %>% select(1)
```
→ On récupère ici `row_order` un data.frame a une seule colonne avec le nom des périodes dans l'ordre définit par la sériation

> ATTENTION! résultats différents mais cycliques à chaque execution regarder de près la fonction `seriation:seriate` 

Mais est-ce vraiment une bonne idée de sérier des périodes ? Il faudrait éventuellement voir le résultat avec seulement une sériation à partir des groupes techniques.

#### sériation des colonnes (groupes techniques)
```{r}
col_order <- as.vector(unlist(seriate(dist(t(tab2), "minkowski", p = 1),  method ="TSP")))
col_order <- tab %>% select(col_order+1) %>% names()
```
→ On récupère ici `col_order` un vecteur avec le nom des groupes techniques dans l'ordre définit par la sériation

### Composition du graphique

Démarche:

* transformer le tableau de contingence en tableau long `gather`
* faire un graphique `ggplot`de cercles proportionnels avec:
  * les groupes_tech(niques) en X
  * les periodes en Y
  * la taille des cercles selon l'effectif
  
```{r}
tab %>%
  gather(groupes_tech, effectif, ind:groupe.4p, factor_key=TRUE) %>%
  ggplot() +
  aes(x=groupes_tech, y=periodes, size=effectif) +
  scale_size(range=c(3,40)) +
  aes(color=periodes) +
  theme(axis.text.x = element_text(angle = 45, hjust=1), legend.position = "bottom") +
  geom_point(alpha = 0.5) +
  guides(color = FALSE) +
  scale_y_discrete(limits=row_order$periodes) +
  scale_x_discrete(limits=col_order)
```
  
### Conclusion:

Cela fonctionne et j'espère avoir répondu à la question !
Cependant, la fonction `seriation:seriate` donne des résultats variables -mais cycliques.

il faudrait donc:

* réexecuter le code pour visualiser les différentes sériations effectuées
* étudier la documentation pour comprendre son fonctionnement (nottamment les différentes méthodes et paramètres



 
