# Summary



## Stat1 : Consolidation, Analyse et Représentation des données archéologiques

* [module socle 1. 0 - Les données archéologiques : préparer et consolider ses tableaux](/mod_1_0_prepadata/mod_1_0_prepadata.md)


* [module socle 1.1  - Les données archéologiques : décrire, analyser et représenter (1 variable) ](/mod1_1_stat/mod1_1_stat.md)

* [module socle 1.2 - Les données archéologiques : initiation aux traitements de données sous R (1 variable) ](/mod1_2_initR/mod1_2_initR.md)



## Ressources

* [Document récapitulant outils et ressources pour l'analyse de données en aRchéologie](AnalyseDonneesArcheo_Ressources.md)

