# Stat 1 : Consolidation, Analyse et Représentation des données archéologiques

Cette formation interne à l'Inrap  est anciennement intitulée **"Initiation aux statistiques descriptives univariées"** a fait l'objet d'une réécriture et se décline désormais en **3 modules socles**



### ![stat_clean.png](images/stat_clean.png)  module socle 1.0 - Les données archéologiques : préparer et consolider ses tableaux

Ce module socle est destiné à tous les agents ayant des données sous la forme de tableau a créer et/ou traiter. Il est un socle commun préalable et indispensable pour suivre toutes les formations Stat et SIG 

**objectifs:** 

* Préparer et consolider des données archéologiques dans un tableau élémentaire en vue d'un futur traitement statistique .
* Utiliser un tableur pour nettoyer, consulter et manipuler des données (depuis le tri simple jusqu'au tableau croisé dynamique)
* Acquérir le vocabulaire simple de description et de caractérisation d'un tableau de données. 

**logiciels utilisés:** tableur (Libre Office)

**durée**: 1 a 2 jours

**prérequis:** aucun ! ce module est destiné à tous les agents. Il est en revanche un prérequis pour toutes les formations SIG et Stat.

> <u>Note:</u> ce module est une réécriture d'une partie de l'ancienne formation Stat 1. 



### ![mod11_stat](images/mod11_stat.png)**module socle 1.1** - Les données archéologiques : décrire, analyser et représenter (1 variable)

**objectifs:** 

* Traiter une colonne d'un tableau élémentaire contenant des **données qualitatives**:
  * description de la variable
  * tableau de synthèse
  * représentations graphiques
* Traiter une colonne d'un tableau élémentaire contenant des **données quantitatives**:
  * description de la variable
  * résumé statistique
  * tableau de synthèse
  * représentations graphiques
* Acquérir le vocabulaire simple de la statistique descriptive.

**logiciels utilisés:** tableur (Libre Office)

**durée**: 2 à 3  jours

**prérequis:** module  socle 1. Ce module est destiné à tous les agents désirant un rappel/une initiation au traitement de données simple (1 seule variable).



###  ![mod12_R](images/mod12_R.png)**module socle 2.1** - Les données archéologiques : initiation aux traitements de données sous R (1 variable)

**objectifs:** 

* A travers les concepts et les exemples des modules précédents, découvrir l'utilisation d'un logiciel dédié au traitement de données:
  * découvrir l'interface du logiciel
  * importer des tableaux
  * traiter une variable qualitative d'un tableau
  * traiter une variable quantitative d'un tableau
  * faire des représentations graphiques appropriées

**logiciels utilisés:** R (logiciel de statistique), Rstudio (interface ergonomique pour R)

**durée**: 2 à 3  jours

**prérequis:** module  socle 1 & 2. Ce module est destiné à tous les agents souhaitant découvrir un logiciel de statistique pour traiter de manière efficace et reproductible des données archéologiques.



##  Ressources:

* Le diaporama associé à cette formation est disponible en ligne (*work in progress*):

[![Diaporama](images/slides.png)](https://slides.com/archeomatic/socle/)


* Les [supports de formation](https://formationsig.gitlab.io/stat1/) en ligne (existent aussi en version PDF, cf. lien dédié en haut de page). 



* Un [document récapitulant](AnalyseDonneesArcheo_Ressources.md) des outils et ressources utiles pour l'**Analyse de données en aRchéologie**



[ :leftwards_arrow_with_hook: Accueil - Formation SIG](https://formationsig.gitlab.io/toc/)



Toute cette documentation est sous licence sous licence [![CC](images/CC_BY_ND.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)