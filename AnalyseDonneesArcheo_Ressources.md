# ![aRcheo](http://isa.univ-tours.fr/IMG/gif/.gif) Analyse de données en aRchéologie 

Ce document a vocation a donner des pistes pour découvrir, se former, lire et pratiquer l'analyse de données en archéologie, de préférence avec les logiciels [R](https://www.r-project.org/) et [Rstudio](https://rstudio.com/).

Il a été créé pour accompagner les stagiaires du stage d'Initiation aux statistiques univariées de l'Inrap pour lequel vous pouvez consulter le [diaporama](https://slides.com/archeomatic/stat1#/)

## Commencer avec R et Rstudio

Rarement dédiées à l'archéologie, il existe de nombreuses ressources en ligne pour se former aux statistiques et à l'analyse de données:



* l'indispensable[ Introduction à R et au *tidyverse*](https://juba.github.io/tidyverse/) de Julien Barnier. L'équivalent  en ligne de 336 pages claires et concises ! très complet pour commencer avec R, Rstudio, les stats uni et bi-variées. Ce document est mis à jour régulièrement et peut être téléchargé en pdf.

  
  
* Le site des [formations à R](https://mtes-mct.github.io/parcours-r/) du ministère de la Transition Écologique et Solidaire


- Dans la même veine et pour compléter: [Analyse-R - Introduction  à l’analyse d’enquêtes  avec R et RStudio](http://larmarange.github.io/analyse-R/). L'équivalent de 1092 pages d'un cours en ligne diviser en 3 rubriques: Manipuler, Analyser et Approfondir. Version téléchargeable en PDF pour les papierophiles.

  

- [R pour statophobes](https://perso.univ-rennes1.fr/denis.poinsot/Statistiques_ pour_statophobes/R pour les statophobes.pdf) (PDF) de Denis Poinsot (l'auteur du célèbre [Statistiques pour statophobes](https://perso.univ-rennes1.fr/denis.poinsot/Statistiques_ pour_statophobes/STATISTIQUES POUR STATOPHOBES.pdf) (pdf) ou comment se marrer avec des stats). A peine 36 pages pour commencer avec R et faire des stats univariées et bi-variées.

- Pour des statistiques amusantes on n'oublie pas le [blog de Lise Vaudor](http://perso.ens-lyon.fr/lise.vaudor/), son [grimoire pour apprendre les stats](http://perso.ens-lyon.fr/lise.vaudor/grimoire/) avec R mais aussi des princesses :princess:  et des dragons :dragon:  et depuis peu sa chaîne [youtube](https://www.youtube.com/channel/UC9QOCBVGHp4gnmcmkG1VbGg).

- Plus près des archéologues, [Éléments de statistiques](http://www.pacea.u-bordeaux.fr/Cours-de-statistique-en-M1-AbP.html) le cours pour les M1 de Bordeaux par Frédéric Santos (PACEA).

  

- pour finir: Faites le formidable Mooc (Cours en Ligne) [Introduction à la statistique avec R](https://www.fun-mooc.fr/courses/UPSUD/42001S06/session06/about). 

  >  Attention:  Il est préférable de commencer à la date de lancement d'une session et compter plusieurs heures sur 5 semaines avec exercices (QCM et devoirs  corrigés -à corriger-  par ses pairs). Pédago exceptionnelle !  



## Analyse de données multidimensionnelles

* Avant de se lancer à corps perdus dans l'analyse de données multidimensionnelles il est conseillé commencer [ par lire ceci ](http://www.sthda.com/french/wiki/analyse-factorielle-des-correspondances-avec-r) pour comprendre l'AFC et la simplicité de la mettre en œuvre dans R (on peut faire [la même chose pour les ACP](http://www.sthda.com/french/wiki/analyse-en-composante-principale-avec-r))
* Aussi il est possible de suivre la formation de Bruno Desachy **analyse des données archéologiques** - statistiques multidimensionnelles appliquées aux tableaux de données qualitatives ou hétérogènes (tableaux de comptage). En attendant vous pouvez lire le [document support](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=2ahUKEwi8wreNt5_pAhXIAWMBHWLuC00QFjAAegQIAhAB&url=https%3A%2F%2Fabp.hypotheses.org%2Ffiles%2F2017%2F07%2Fsupport_17-07.pdf&usg=AOvVaw3AOyztKVJUsiNMPbT9iYlW) (pdf)



- le site [Analyse SHS](http://analyse.univ-paris1.fr/) de Paris 1 pour faire du R sans s'en rendre compte via une interface avec des boutons ;) (en plus on peut récupérer le code R pour comprendre et reproduire la démarche avec R) !!



- Le package (FR=) [FactomineR](http://factominer.free.fr/index_fr.html) est fait pour vous. Il existe même des vidéos sur la [chaîne Youtube de l'auteur](https://www.youtube.com/user/HussonFrancois)  François Husson (Agrocampus Rennes).

- Il existe aussi le package [ade4](http://pbil.univ-lyon1.fr/ade4/accueil.php) (labo de bio - UMR5558 - Univ. Lyon 1)

- Pour compléter, simplifier (et faire des graphiques multidimensionnels justes et jolis) avec une interface d'exploration ”interactive”,  jetez vous sur le package [explor-R](https://cran.r-project.org/web/packages/explor/vignettes/introduction_fr.html) (vignette d'explication) de J.Barnier, encore lui.

  

- Il existe aussi y a aussi un Mooc « [Analyse des données multidimensionelles](https://www.fun-mooc.fr/courses/agrocampusouest/40001S02/session02/about) » d'Agrocampus Ouest (Rennes) avec utilisation de FactomineR.



## Sériations

- Pour les sériations de matrice graphique vous pouvez évidemment regarder les outils créés par Bruno Dessachy notamment le dernier en date : [l'Explographe](https://cours.univ-paris1.fr/course/view.php?id=7892) (sous la forme d'un classeur LibreOffice Calc).
- Pour une première approche des sériations avec R, un cours article avec du code intitulé [Sériation dans R : Comment Ordonner de Manière Optimale les Objets dans une Matrice de Données](https://www.datanovia.com/en/fr/blog/seriation-dans-r-comment-ordonner-de-maniere-optimale-les-objets-dans-une-matrice-de-donnees/)sur le site datanovia.com

* Il existe un package dédié à la sériation en archéologie -y compris celles de B.Dessachy (Sériographe et Matrigraphe): [tabula](https://tabula.archaeo.science/) par Nicolas Frerebeau (Univ. Bordeaux - IRAMAT)



## Ressources générales pour R



- Pour avoir une vision des possibilités graphiques avec R on garde toujours dans un coin de son navigateur la page [conception des graphiques avec R](http://www.aliquote.org/articles/tech/r-graphics/index.html.old) du site aliquote.fr et [Graphiquesavec R](http://zoonek2.free.fr/UNIX/48_R_2004/04.html) du site zoonek2.free.fr.



## R et archéologie

- (ENG) Il est toujours bon de prendre du temps pour explorer la [CRAN Task View](https://github.com/benmarwick/ctv-archaeology) (catalogue des packages R dédiés à une discipline)  dédiée à l'archéologie Science maintenue par Ben Marwick qui enseigne l'archéologie à l'université de Washington et qui se veut la référence dans l'utilisation de R en archéologie.  



## Bibliographie



- (ENG) L'indispensable [Notes on Quantitative Archaeology and R](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwjqpOe6vJ_pAhVl6uAKHUdzBkAQFjACegQIARAB&url=http%3A%2F%2Fwww.mikemetrics.com%2Fdownload%2Fi%2Fmark_dl%2Fu%2F4011023365%2F4623239688%2FBook.pdf&usg=AOvVaw2PELBmKPotSmizv3vnMnDb) de Mike Baxter accompagné des données sources pour reproduire les exemples !



## Outils divers

- Une petite application d'aide à la discrétisation : [Discretisator](https://archeomatic.shinyapps.io/Discretisator/) (en construction, [les sources sont disponible sur GitHub](https://github.com/archeomatic/Discretisator/blob/master/server.R), on peut aussi y accéder directement dans Rstudio en copiant les lignes suivantes :

  ```R
  install.packages("shiny")
  library(shiny)
  runGitHub("discretisator", "archeomatic") 	
  ```

* Voir aussi [exploratR](https://github.com/hcommenges/ExploratR) plus propre avec la cartographie incluse mais moins de méthodes de discrétisation.
  ```R
runGitHub("exploratR", "hcommenges") 
  ```



* Quand on parle analyse des données et archéologie on ne peut pas oublier:
  * L'[Atelier SITraDA](https://sitrada.hypotheses.org/) Systèmes d'information et traitements de données archéologiques (Paris I - ArchéoFab - Arscan UMR7041)
  * Les [Ateliers Archéomatiques](http://isa.univ-tours.fr/spip.php?article373) (Réseau ISA - Inrap - UMR7324-LAT )

# Exemples de publications

[Bordeaux (33), 12 rue Jean Fleuret, 18 cours du Maréchal Juin - Du  cuir et des clous : Traitement des peaux durant l'Antiquité à Burdigala : rapport de fouille - Vanessa Elizagoyen](http://dolia.inrap.fr/flora/ark:/64298/0152999)

[Analyser et regrouper des périodes chronologiques :  applications statistiques en archéologie avec R et Rstudio](https://github.com/JGravier/sitRada/tree/master/AA2018) un atelier archéomatique de Julie Gravier (Univ. Paris I - UMR 8504), script R et données comprises.